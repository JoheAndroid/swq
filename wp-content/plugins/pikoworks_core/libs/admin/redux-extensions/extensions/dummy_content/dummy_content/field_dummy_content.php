<?php
/**
 * Redux Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * any later version.
 *
 * Redux Framework is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Redux Framework. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package     ReduxFramework
 * @author      Dovy Paukstys
 * @version     3.1.5
 */
// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) exit;


// Don't duplicate me!
if( !class_exists( 'ReduxFramework_dummy_content' ) ) {

    /**
     * Main ReduxFramework_dummy_content class
     *
     * @since       1.0.0
     */
    class ReduxFramework_dummy_content extends ReduxFramework {

        /**
         * Field Constructor.
         *
         * Required - must call the parent constructor, then assign field and value to vars, and obviously call the render field function
         *
         * @since       1.0.0
         * @access      public
         * @return      void
         */
        function __construct( $field = array(), $value ='', $parent ) {

            $this->parent = $parent;
            $this->field = $field;
            $this->value = $value;

            if ( empty( $this->extension_dir ) ) {
                $this->extension_dir = trailingslashit( str_replace( '\\', '/', dirname( __FILE__ ) ) );
                $this->extension_url = site_url( str_replace( trailingslashit( str_replace( '\\', '/', ABSPATH ) ), '', $this->extension_dir ) );
            }

            // Set default args for this field to avoid bad indexes. Change this to anything you use.
            $defaults = array(
                'options'           => array(),
                'stylesheet'        => '',
                'output'            => true,
                'enqueue'           => true,
                'enqueue_frontend'  => true
            );
            $this->field = wp_parse_args( $this->field, $defaults );

        }

        /**
         * Field Render Function.
         *
         * Takes the vars and outputs the HTML for the field in the settings
         *
         * @since       1.0.0
         * @access      public
         * @return      void
         */
        public function render() {
            $versions_imported = get_option('versions_imported');

            if( empty( $versions_imported ) ) $versions_imported = array();

            $class = '';

            if( ! in_array( 'default', $versions_imported ) ) {
                $class = ' no-default-imported'; 
            }

            foreach($versions_imported as $ver) {
                $class = ' imported-' . $ver;
            }

            echo '</td></tr></table><div class="pikoworks-import-section' . esc_attr( $class ) . '">';
//           
            $versions = require PIKOWORKSCORE_CORE . 'importer/versions.php';

            $pages = array_filter($versions, function( $el ) {
                return $el['type'] == 'page';
            });

            $demos = array_filter($versions, function( $el ) {
                return $el['type'] == 'demo';
            });

            ?>
            <div class="loading-info">
                <h2>Dummy importing, max time take up to 3 minutes.</h2>
                <div class="piko-loader">                    
                    <svg viewBox="0 0 100 100" preserveAspectRatio="xMidYMid">
                        <circle cx="50" cy="50" ng-attr-r="{{config.radius}}" ng-attr-stroke-width="{{config.width}}" ng-attr-stroke="{{config.c1}}" ng-attr-stroke-dasharray="{{config.dasharray}}" fill="none" stroke-linecap="round" r="40" stroke-width="6" stroke="#030303" stroke-dasharray="62.83185307179586 62.83185307179586">
                          <animateTransform attributeName="transform" type="rotate" calcMode="linear" values="0 50 50;360 50 50" keyTimes="0;1" dur="1.9s" begin="0s" repeatCount="indefinite"></animateTransform>
                        </circle>
                        <circle cx="50" cy="50" ng-attr-r="{{config.radius2}}" ng-attr-stroke-width="{{config.width}}" ng-attr-stroke="{{config.c2}}" ng-attr-stroke-dasharray="{{config.dasharray2}}" ng-attr-stroke-dashoffset="{{config.dashoffset2}}" fill="none" stroke-linecap="round" r="33" stroke-width="6" stroke="#d39182" stroke-dasharray="51.83627878423159 51.83627878423159" stroke-dashoffset="51.83627878423159">
                          <animateTransform attributeName="transform" type="rotate" calcMode="linear" values="0 50 50;-360 50 50" keyTimes="0;1" dur="1.9s" begin="0s" repeatCount="indefinite"></animateTransform>
                        </circle>
                    </svg>
                </div>
            </div>

            <h3>
                <?php esc_html_e( 'Import base content', 'pikoworks_core'); ?>
            </h3>

            <?php if( ! in_array( 'default', $versions_imported ) ): ?>
                <div class="pikoworks-import-info">
                    <strong>Import Base Content</strong><br>
                    <p>Start working with our template by installing base demo content. Then you will get the opportunity to install the Home Page and other demo the provided below list.</p>
                    <a href="#" class="piko-button button-import-default button-import-version" data-version="default">
                        <?php esc_html_e('Import base dummy content', 'pikoworks_core'); ?>
                    </a>
                </div>
            <?php endif; ?>

            <div class="pikoworks-imported-info">
                <strong>Base Content Data Installed!</strong>
                <br>
                <p>You have successfully imported base demo content. Now you can import additional page. </p>
            </div>

            <div class="import-demos-wrapper">
                <h3><?php esc_html_e( 'Import demo versions', 'pikoworks_core'); ?></h3>
                <div class="import-demos">
                    <?php foreach ($demos as $key => $version): ?>
                        <div class="version-preview <?php echo ( in_array( $key, $versions_imported ) ) ? 'version-imported' : 'not-imported'; ?> version-preview-<?php echo esc_attr( $key ); ?>">
                            <div class="version-screenshot">
                                <img src="<?php echo PIKOWORKSCORE_CORE_URL . 'importer/dummy/' . $key . '/screenshot.jpg'; ?>" alt="">
                                <a href="<?php echo esc_url( $version['preview_url'] ); ?>" target="_blank" class="button-preview">
                                    <?php esc_html_e('Live prview', 'pikoworks_core'); ?>
                                </a>
                                <a href="#" class="piko-button button-import-version button-import-version" data-version="<?php echo esc_attr( $key ); ?>">
                                    <?php echo ( ! in_array( $key, $versions_imported ) ) ? esc_html__('Import demo', 'pikoworks_core') : esc_html__('Activate', 'pikoworks_core') ; ?>
                                </a>
                                <span class="installed-icon"><?php esc_html_e('Data imported', 'pikoworks_core'); ?></span>
                            </div>
                            <span class="version-title"><?php echo esc_html( $version['title'] ); ?></span>
                        </div>
                    <?php endforeach ?>
                </div>
                <div class="install-base-first">
                    <h3><?php esc_html_e('Access Denied!', 'pikoworks_core'); ?></h3>
                    <p><?php esc_html_e('Please, install Base demo content before, to access the collection of our Home Pages.', 'pikoworks_core'); ?></p>
                </div>
            </div>

            <div class="import-additional-pages">
                <h3><?php esc_html_e( 'Import additional pages', 'pikoworks_core'); ?></h3>

                <div class="page-preview">
                    <img src="<?php echo PIKOWORKSCORE_CORE_URL . 'importer/dummy/faq/screenshot.jpg'; ?>" alt="">
                    <a href="<?php echo $pages['faq']['preview_url']; ?>" target="_blank" class="preview-page-button">
                        <?php esc_html_e('Live preview', 'pikoworks_core'); ?>
                    </a>
                </div>
                <div class="page-selector">
                    <select name="pages-selector" id="pages-selector" data-url="<?php echo PIKOWORKSCORE_CORE_URL . 'importer/dummy/'; ?>">
                        <?php foreach ($pages as $key => $version): ?>
                            <option value="<?php echo esc_attr( $key ); ?>" data-preview="<?php echo $version['preview_url']; ?>"><?php echo esc_html( $version['title'] ); ?></option>
                        <?php endforeach ?>
                    </select>

                    <a href="#" class="piko-button button-import-page button-import-version" data-version="faq">
                        <?php esc_html_e('Import', 'pikoworks_core'); ?>
                    </a>

                    <div class="pikoworks-options-info">
                        <b>Import Additional Pages</b><br>
                        NB: Please these pages should be added to your menu via Appearance>Menus. And configure your self.
                    </div>
                    <div class="pikoworks-options-info info-red">
                        Attention! Before additional pages import, please, do the backup of your Theme Settings: "Import / Export - Options" and your web site entirely.
                    </div>
                </div>
            </div>

            <?php

            echo '</div><table class="form-table no-border" style="margin-top: 0;"><tbody><tr style="border-bottom:0; display:none;"><th style="padding-top:0;"></th><td style="padding-top:0;">';

        }

        /**
         * Enqueue Function.
         *
         * If this field requires any scripts, or css define this function and register/enqueue the scripts/css
         *
         * @since       1.0.0
         * @access      public
         * @return      void
         */
        public function enqueue() {

            // $extension = ReduxFramework_extension_dummy_content::getInstance();

            wp_enqueue_script(
                'redux-field-dummy-content-js',
                $this->extension_url . 'field_dummy_content.js',
                array( 'jquery' ),
                time(),
                true
            );

            wp_enqueue_style(
                'redux-field-dummy-content-css',
                $this->extension_url . 'field_dummy_content.css',
                time(),
                true
            );

        }

        /**
         * Output Function.
         *
         * Used to enqueue to the front-end
         *
         * @since       1.0.0
         * @access      public
         * @return      void
         */
        public function output() {

            if ( $this->field['enqueue_frontend'] ) {

            }

        }

    }
}
