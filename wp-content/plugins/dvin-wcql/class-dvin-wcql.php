<?php


/**
 * Dvin_Wcql class, Handles the core Quotelist functionality
 */
 class Dvin_Wcql {
 
	public $errors_arr; //stores the errors
	public $details_arr; //stores details array
	public $messages_arr; //stores details array
    public $order_id;
	
	/**
	* Constructor initializes the variables with respective values
	*@access public
	*/
	public function __construct($details_arr) {
		$this->errors_arr = array();
		$this->details_arr = $details_arr;
        $order_id = 0;
	}
	
	/**
   * function isExists, checks for existence of the product
	 * @access  public
	 * @boolean
   */	
	public function isExists($prod_id) {
	
		global $wpdb,$dvin_qlist_products;
        if(is_array($dvin_qlist_products))
            if(array_key_exists( $prod_id, $dvin_qlist_products))
                return true;
        return false;		
	}
	
	/**
   * function remove, removes product from quotelist based on its ID
	 * @access  public
	 * @boolean 
   */	
	public static function remove($id) {
	   global $dvin_qlist_products;
        unset($dvin_qlist_products[$id]);
        WC()->session->set('dvin_qlist_products',$dvin_qlist_products);
        return true;
	}	
	
	/**
	* function get_url, Builds the quotelist page URL
	* @access  public
	* @return string URL 
  */		
	public static function get_url() {
		global $dvin_wcql_settings;
		if(isset($dvin_wcql_settings['dvin_wcql_quotelist_url']) && !empty($dvin_wcql_settings['dvin_wcql_quotelist_url']))
			return $dvin_wcql_settings['dvin_wcql_quotelist_url'];
		return get_permalink(get_option('dvin_quotelist_pageid'));
	}
	
	
	/**
	* function get_remove_url, Builds the generic URL which handles removal of product from quotelist
	* @access  public
	* @string URL 
  */	
	public static function get_remove_url($id) {
		return add_query_arg('action','remove_from_quotelist',add_query_arg('qlist_item_id', $id,DVIN_QLIST_PLUGIN_WEBURL."dvin-wcql-ajax.php"));
	}
	
/**
	* function add, Adds product to quotelist
	* @access  public
	* @return string "error" or "true"  , "error" indicates, error occurred
  */
	public function add() {
        
        //adsjust the price
	
		global $wpdb,$woocommerce,$dvin_qlist_products;
        $dvin_qlist_products = self::get_qlist_from_session();
        
        //handle simple product
		if (!isset($this->details_arr['variation_id']) && isset($this->details_arr['product_id'])) :
		//single product
			$quantity = (isset($this->details_arr['quantity'])) ? (int) $this->details_arr['quantity'] : 1;
			
			// check for existence,  product ID, variation ID, variation data, and other cart item data
			if($this->isExists( $this->details_arr['product_id'], '')) {
				return "exists";
			}
			
            //get price
            $index=$price=0;
            $price = get_quotelist_product_price( wc_get_product($this->details_arr['product_id']) );
        
			//add to session
			$dvin_qlist_products[$this->details_arr['product_id']]['product_id'] = $this->details_arr['product_id'];
			$dvin_qlist_products[$this->details_arr['product_id']]['quantity'] = $quantity;
            if(isset($this->details_arr['addons'])) {
                $dvin_qlist_products[$this->details_arr['product_id']]['addons'] = $this->details_arr['addons'];

                //add addons price
                foreach($this->details_arr['addons'] as $ar) {
                   $price += (float)$ar['price'];                             
                }
               
            }
            $dvin_qlist_products[$this->details_arr['product_id']]['price']=$price;
        
            WC()->session->set('dvin_qlist_products',$dvin_qlist_products);
			$ret_val = true;
			return "true";
			
	
		elseif (isset($this->details_arr['variation_id']) && isset($this->details_arr['product_id'])) :
			
			// Variation add to cart
			if (empty($this->details_arr['variation_id']) || !is_numeric($this->details_arr['variation_id'])) :
				
				$this->errors_arr[]= __('Please choose product options&hellip;', 'dvinwcql');
				return "error";
		
		   else :
				
				$product_id 	= (int) $this->details_arr['product_id'];
				$variation_id 	= (int) $this->details_arr['variation_id'];
				$quantity 		= (isset($this->details_arr['quantity'])) ? (int) $this->details_arr['quantity'] : 1;
				
				$attributes = (array) maybe_unserialize(get_post_meta($product_id, '_product_attributes', true));
				$variations = array();
				$all_variations_set = true;
				foreach ($attributes as $attribute) :
					if ( !$attribute['is_variation'] ) continue;
					$taxonomy = 'attribute_' . sanitize_title($attribute['name']);
					if (!empty($this->details_arr[$taxonomy])) :
						// Get value from post data
						$value = esc_attr(stripslashes($this->details_arr[$taxonomy]));
						// Use name so it looks nicer in the cart widget/order page etc - instead of a sanitized string
						$variations[esc_attr($attribute['name'])] = $value;
					else :
						$all_variations_set = false;
					endif;
				endforeach;
				if ($all_variations_set && $variation_id > 0) :
					
					
					// check for existence,  product ID, variation ID, variation data, and other cart item data
					if($this->isExists($variation_id)) {
						return "exists";
					}
                    
					//push the variable data into session
					$this->details_arr['variation_data'] = serialize($variations);
                    ///get price
                    $price=0;
                    $price = get_quotelist_product_price( wc_get_product($variation_id) );

                    if(isset($this->details_arr['addons']) && is_array($this->details_arr['addons'])) {
                        foreach($this->details_arr['addons'] as $ar) {
                           $price += (float)$ar['price'];                             
                        }
                    }
                    $this->details_arr['price']=$price;
        
					$dvin_qlist_products[$variation_id]=$this->details_arr;
                    WC()->session->set('dvin_qlist_products',$dvin_qlist_products);
					$ret_val = true;
				
					//if added, return true, otherwise error
					if($ret_val) :
						return "true";
					else :
						$this->errors_arr[]= __('Error occurred while adding product to quotelist','dvinwcql');
						return "error";
					endif;
				else :
					$this->errors_arr[]= __('Please choose product options&hellip;', 'dvinwcql') ;
					return "error";
				endif;
				
			endif; 
		
		endif;
	}
	
	/**
	* function send_email, Sends Email
	* @access  public static
	* @return boolean 
  */
	public function send_email() {
		
        global $dvin_qlist_products,$dvin_wcql_settings;
        
		$overall_tot_price = 0;
		$email = get_option('dvin_wcql_admin_email');
		$postfix_email = get_option('dvin_wcql_admin_postfix_email');
		$subject = get_option('dvin_wcql_email_subject');
		$message = get_option('dvin_wcql_email_msg');
		$cc = get_option('dvin_wcql_copy_toreq');
        
		list($overall_tot_price,$quote_list) = get_qlist_table(array());
			
		 $overall_tot_price_str = isset($overall_tot_price)?apply_filters('woocommerce_cart_item_price_html', woocommerce_price( $overall_tot_price)):'';			
	
		$needle_arr = array('[%req_name%]','[%req_email%]','[%quotelist%]','[%total_price%]','[%comments%]');
		$replace_with_arr = array(ucwords($_POST['req_name']),$_POST['req_email'],$quote_list,$overall_tot_price_str,$_POST['req_details']);
		
		//apply filters  to capture custom fileds names in [%filed_name%]
		$needle_arr = apply_filters('dvin_wcql_custom_fields_needles',$needle_arr);
		
		//apply filters for 
		$replace_with_arr = apply_filters('dvin_wcql_custom_fields_replacements',$replace_with_arr);
        
		//include the validation file if exists
		if(file_exists(TEMPLATEPATH . '/'. Dvin_Wcql::template_path().'custom-field-arr.php')) {
			include(TEMPLATEPATH . '/'. Dvin_Wcql::template_path().'custom-field-arr.php');
		}elseif(file_exists(STYLESHEETPATH . '/'. Dvin_Wcql::template_path().'custom-field-arr.php')) {
			include( STYLESHEETPATH . '/'. Dvin_Wcql::template_path().'custom-field-arr.php');
		}
		$subject = str_replace($needle_arr,$replace_with_arr,$subject);
		$message = str_replace($needle_arr,$replace_with_arr,$message);
        
        $message ='<html><body><style>table, th, td{border: 1px solid black;}</style>'.$message.'</body></html>';
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
		
		// Additional headers
		$from_admin_email = !empty($postfix_email)? $postfix_email : $_POST['req_name'].'<'.$_POST['req_email'].'>';
		$to_admin_headers = $headers.'From: '.$from_admin_email."\r\n";
		$to_admin_headers = $to_admin_headers.'Reply-to: '.$_POST['req_name'].'<'.$_POST['req_email'].">\r\n";
		//check for whether to send copy to customer
		if($cc == "on"){
			$to_customer_headers = $headers.'From: '.$email."\r\n";
		}
        
        //get order info. in case of admin email
        $admin_message = str_replace('[%orderinfo%]', $this->get_order_info(),$message);
        
        //replace with nothing in case of client
        $message = str_replace('[%orderinfo%]','',$message);
        
		//send the email 
		if(wp_mail($email, $subject, nl2br($admin_message),$to_admin_headers)) {
            
			if($to_customer_headers!=''){
				if(wp_mail($_POST['req_email'], $subject, nl2br($message),$to_customer_headers)){
                    $dvin_qlist_products = array();
					WC()->session->set('dvin_qlist_products',array());
					return true;
				}							
			} else {
                $dvin_qlist_products = array();
                WC()->session->set('dvin_qlist_products',array());
				return true;
			}
		}
		return false;
	}
	
		
	/**
	 * Get the template path.
	 *
	 * @return string
	 */
	public static function template_path() {
		return apply_filters( 'DVIN_QLIST_TEMPLATE_PATH', 'dvin-wcql/');
	}
     
     public function get_order_info() {
         
         global $dvin_qlist_products;
        
         //for admin message, replace the order info with right info otherwise space
        $order_info = apply_filters('dvin_wcql_order_info_str',__('Order Links(%s):<a href="%s">View</a>&nbsp;|&nbsp;<a href="%s">Edit</a>&nbsp;|&nbsp;<a href="%s">CheckOut</a>','dvinwcql'));
        $order_obj = new WC_Order($this->order_id);
        $order_edit_url = admin_url( 'post.php?post=' . absint( $this->order_id ) . '&action=edit');
        //$order_obj->get_view_order_url();
        return sprintf($order_info,$this->order_id,$order_obj->get_view_order_url(),$order_edit_url,$order_obj->get_checkout_payment_url());
     }
     
     //load all ajax actions
     public static function load_ajax_actions() {
        add_action( 'wp_ajax_wcql_widget_refresh',array('Dvin_Wcql_Ajax','widget_refresh'));
        add_action( 'wp_ajax_nopriv_wcql_widget_refresh',array('Dvin_Wcql_Ajax','widget_refresh'));
        add_action( 'wp_ajax_add_to_qlist',array('Dvin_Wcql_Ajax','add_to_qlist'));
        add_action( 'wp_ajax_nopriv_add_to_qlist',array('Dvin_Wcql_Ajax','add_to_qlist'));
        add_action( 'wp_ajax_remove_from_qlist',array('Dvin_Wcql_Ajax','remove_from_qlist'));
        add_action( 'wp_ajax_nopriv_remove_from_qlist',array('Dvin_Wcql_Ajax','remove_from_qlist'));
        add_action( 'wp_ajax_find_prod_in_qlist',array('Dvin_Wcql_Ajax','find_prod_in_qlist'));
        add_action( 'wp_ajax_nopriv_find_prod_in_qlist',array('Dvin_Wcql_Ajax','find_prod_in_qlist'));
        add_action( 'wp_ajax_add_to_qlist_from_prodpage',array('Dvin_Wcql_Ajax','add_to_qlist'));
        add_action( 'wp_ajax_nopriv_add_to_qlist_from_prodpage',array('Dvin_Wcql_Ajax','add_to_qlist'));
        add_action( 'wp_ajax_remove_from_page',array('Dvin_Wcql_Ajax','remove_from_page'));
        add_action( 'wp_ajax_nopriv_remove_from_page',array('Dvin_Wcql_Ajax','remove_from_page'));
        add_action( 'wp_ajax_update_list',array('Dvin_Wcql_Ajax','update_list'));
        add_action( 'wp_ajax_nopriv_update_list',array('Dvin_Wcql_Ajax','update_list'));
        add_action( 'wp_ajax_send_request',array('Dvin_Wcql_Ajax','send_request'));
        add_action( 'wp_ajax_nopriv_send_request',array('Dvin_Wcql_Ajax','send_request'));
     }
     
     //localisation
     public static function add_localisation() {
        //define constants if not
        if(!defined('TEMPLATEPATH_QLIST'))
            define('TEMPLATEPATH_QLIST',get_template_directory());
        if(!defined('STYLESHEETPATH_QLIST'))
            define('STYLESHEETPATH_QLIST',get_stylesheet_directory());
        //include the language file
        if(file_exists(TEMPLATEPATH_QLIST . '/'. Dvin_Wcql::template_path().'languages/')) {
            $langdir_path = TEMPLATEPATH_QLIST . '/'. Dvin_Wcql::template_path().'languages/';
        }elseif(file_exists(STYLESHEETPATH_QLIST . '/'. Dvin_Wcql::template_path().'languages/')) {
            $langdir_path = STYLESHEETPATH_QLIST . '/'. Dvin_Wcql::template_path().'languages/';
        }else{
            $langdir_path =  dirname( plugin_basename( __FILE__ ) ). '/languages/';
        }
        load_plugin_textdomain( 'dvinwcql', false, $langdir_path );             
     }
     
     //ajax includes
     public static function load_ajax_includes() {        
       
       self::add_localisation(); // add languages files        
	   self::load_ajax_actions(); //ajax actions
       require_once(DVIN_QLIST_PLUGIN_URL."class-dvin-wcql-ajax.php"); //core class
     }
     
     //frontend site includes
     public static function load_site_includes() {
         
         require_once(DVIN_QLIST_PLUGIN_URL."class-dvin-wcql-ui.php");//ui related
         require_once("class-dvin-wcql-shortcodes.php"); //shortcodes inclusion
         self::add_localisation(); // add languages files 
     }
      //backend includes
     public static function load_backend_includes() {      
         self::add_localisation(); // add languages files
         include_once(DVIN_QLIST_PLUGIN_URL."class-dvin-wcql-admin.php");	        
     
     }
     //sets session cookies
    public static function set_session_cookies() {
        do_action( 'woocommerce_set_cart_cookies', true );        
    }
     
    //get the qlist from the session
     public static function get_qlist_from_session(){
            if(function_exists('WC') && is_object(WC()->session))
          	  return WC()->session->get('dvin_qlist_products');
			return;
     }
 }
 ?>