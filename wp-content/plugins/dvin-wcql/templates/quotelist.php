<?php

/**
 * Quotelist Page
 */
/*******************START ---- DO NOT TOUCH THIS CODE***********************/
 global $woocommerce,$dvin_wcql_settings, $current_user,$dvin_wcql_obj;
?>
<div id='dvin_message' align="left" style="display:none;color:red;">&nbsp;</div>
<div id="dvin_wcql_success_msg" style="display:none">
	<?php  echo apply_filters('dvin_wcql_ack_text',__('Sent email to Admin for the quote.','dvinwcql')); ?>
</div>
<?php if(wc_notice_count()>0) { ?>
<div id="dvin_wcql_details">
<?php 	wc_print_notices();?>
</div>
<?php } 
if(!isset($_REQUEST['qlist_process'])) {
?>
<div id="content">
<?php if((!isset($dvin_wcql_settings['use_gravity_forms']) || $dvin_wcql_settings['use_gravity_forms']!='on') && (!isset($dvin_wcql_settings['use_contactform7']) || $dvin_wcql_settings['use_contactform7']!='on')) { ?>
<form method="post"  class="qlist" action="">
<?php } ?>
	<input type='hidden' name='qlist_process' value='true'/>
	<input type='hidden' name='action' value='send_request'/>
<?php /*************END ---- DO NOT TOUCH THIS CODE***********************/ ?>
<!-- STARTS LAYOUT --->
    <?php
                                        
      //determine whether to show proce column or not based on settings.
      if((isset($dvin_wcql_settings['remove_price_col'] ) && $dvin_wcql_settings['remove_price_col'] == 'on') || ((isset($dvin_wcql_settings['show_price_login'] ) && $dvin_wcql_settings['show_price_login'] == 'on') && !is_user_logged_in())) {
          $remove_price_col = true;
      } else{
         $remove_price_col = false;
      }
    ?>
    
	<table class="shop_table cart" cellspacing="0">
	<thead>
		<tr>
			<th class="product-remove"></th>
			<th class="product-thumbnail"></th>
			<th class="product-name"><span class="nobr"><?php _e('PRODUCT', 'dvinwcql'); ?></span></th>
			<?php if(!$remove_price_col) { ?>
			<th class="product-price"><span class="nobr"><?php _e('PRICE', 'dvinwcql'); ?></span></th>
			<?php } 
            if($dvin_wcql_settings['show_sku_col'] ==  'on') { ?>
            <th class="product-sku"><span class="nobr"><?php _e('SKU', 'dvinwcql'); ?></span></th>
            <?php }
			if(isset($dvin_wcql_settings['no_qty'] ) && $dvin_wcql_settings['no_qty'] != 'on') { ?>
			<th class="product-quantity"><span class="nobr"><?php _e('QUANTITY','dvinwcql'); ?></span></th>
			<?php } if(!$remove_price_col) { ?>
			<th class="product-price"><span class="nobr"><?php _e('TOTAL', 'dvinwcql'); ?></span></th>
			<?php } ?>
		</tr>
	</thead>
	<tbody>
		<?php	
	
		$colspan = 7;
		//calculate the colspan
		$colspan = $remove_price_col != 'on'?$colspan:$colspan-2;
		$colspan = $dvin_wcql_settings['no_qty'] != 'on'?$colspan:$colspan-1;
        $colspan = $dvin_wcql_settings['show_sku_col'] != 'on'?$colspan:$colspan-1;                       
                                        
        if (sizeof($qlist)>0) { 
		$grand_total_qty = 0;
		$grand_total_price = 0;
        
        if(class_exists('Product_Addon_Cart')) {
            $addon_cart_obj = new Product_Addon_Cart();
        }
            
            
		//loop through the list
		foreach ($qlist as $key => $values) :
        
           if($key =='')
			 continue;
            
			//check for validity of the entry/product
			if(!isset($values['product_id']) && !isset($values['variation_id']))
				continue;
			//initialize to avoid notices
			if(!isset($values['variation_id']))
						$values['variation_id']=0; 
						
			if (isset($values['product_id']) && isset($values['variation_id'])) {
				if(isset($values['product_id']))
					$values['prod_id'] = $values['ID'] = $values['product_id'];
				if(!empty($values['variation_id']))
					$values['ID'] = $values['variation_id'];
			} else{
				$values['prod_id'] = $values['ID'] = $values['product_id'];
				$values['ID'] =  $values['product_id'];
			}
			
			// Get the product
				$product_obj = wc_get_product( $values['variation_id']!=0 ? $values['variation_id'] : $values['product_id'] );
				if ($product_obj->exists()) :
					 $price = 0;					 
					?>
					<tr id="<?php echo $values['ID']?>_dvin_rowid" class="cart-item">
						<td class="product-remove"><a href="javascript:void(0)" title="<?php _e('Remove this item','dvinwcql'); ?>" class="remove removeproductqlist" data-product_id="<?php echo $values['ID'];?>">&times;</a></td>
						<td class="product-thumbnail">
							<a href="<?php echo esc_url( get_permalink(apply_filters('woocommerce_in_cart_product', $values['prod_id'])) ); ?>">
							<?php 
								echo $product_obj->get_image();
							?>
							</a>
						</td>
						<td class="product-name">
							<a href="<?php echo esc_url( get_permalink(apply_filters('woocommerce_in_cart_product', $values['prod_id'])) ); ?>"><?php echo $product_obj->get_title(); ?></a><br/>
							<?php
            
                              
                                
  								if(!empty($values['variation_id'])):
                                    echo wc_get_formatted_variation( $product_obj->variation_data, false );
								endif;	            
            
                                    //get price before showing the addon data as proce could change
                                if(!isset($values['price']))
                                    $values['price'] = get_quotelist_product_price( $product_obj );
            
                                $temp_arr =array();

                                if( isset($addon_cart_obj) && $addon_cart_obj instanceof Product_Addon_Cart) {            
                                $temp_arr = dvin_wcql_get_add_on_list($addon_cart_obj,$values['price'], $product_obj, $values);
            } else {
                                $temp_arr = dvin_wcql_get_add_on_list(array(),$values['price'], $product_obj, $values);
            }
            echo wc_get_formatted_variation($temp_arr,false);                        
                         ?>
						</td>
						<?php if(!$remove_price_col) { ?>
						<td class="product-price">
						<?php 
							$price_in_html = apply_filters( 'woocommerce_cart_product_price', wc_price( $values['price'] ), $product_obj );
							echo apply_filters( 'woocommerce_cart_item_price',$price_in_html,'','');			
						?></td>
                        <?php }//end of checking remove price or not
                        if(isset($dvin_wcql_settings['show_sku_col']) && $dvin_wcql_settings['show_sku_col'] ==  'on') { ?>
                        <td  class="product-sku" valign="center">
							<?php echo $product_obj->get_sku(); ?>
						</td>    
						<?php }//end of adding SKU or not
						if(isset($dvin_wcql_settings['no_qty'] ) && $dvin_wcql_settings['no_qty'] != 'on') { 
								$grand_total_qty += (int)$values['quantity']; 
							?>
						<td  class="product-quantity" valign="center">
							<?php echo dvin_qlist_quantity_input('qty['. $values['ID'].']',$values['quantity'],array(),$product_obj,false); ?>
						</td>
						<?php }  if(!$remove_price_col) { 
								 $grand_total_price += (float) $values['price'] * (int)$values['quantity'];
							?>
						<td class="product-price">
						<?php echo apply_filters('woocommerce_cart_item_price_html', woocommerce_price( (float) $values['price'] * (int)$values['quantity'])); ?>
						</td>
						<?php }//end of checking remove price or not ?>						
					</tr>			
					<?php
				else:
					continue;
				endif;
			endforeach; 
             if(!$remove_price_col) { ?>	
                                    <tr>
                                    <td colspan="<?php echo $colspan-3;?>" align="right"><div  style="float:right;"><?php  echo apply_filters('grand_total_text',__('GRAND TOTAL','dvinwcql')); ?></div></td>
                                        <td align="left" valign="center" style="text-align:center;">&nbsp;<?php echo $grand_total_qty; ?></td>
                                        <td align="right"><?php echo woocommerce_price($grand_total_price); ?></td>
                                    </tr>					
                                <?php
                                    }//end of grand total display
            //if no qty, hide the button                                        
            if(isset($dvin_wcql_settings['no_qty'] ) && $dvin_wcql_settings['no_qty'] != 'on') { 
            ?>
                        <tr>
                                <td colspan="<?php echo $colspan;?>" align="right">	
                                <?php
                                                echo '<div style="float:right;"><button type="button" class="button alt wcqlupdatelistbtn" onClick="javascript:ajax_req_update_quote();">'.apply_filters('update_list_text', __('Update List',"dvinwcql")).'</button></div></div>';	
                                ?>
                                </td>				
                        </tr>
            <?php
            } //end of noqty, updatelist button
        } else {			
		?>
            <tr>
            <td colspan="<?php echo $colspan;?>"><center><?php _e('No products were added to list','dvinwcql') ?></center></td>
            </tr>
		<?php
        } //end of qlist check
		?>
	</tbody>
</table>
</div>
<?php
}//end of if condition qlist_process
?>
<!-- ENDS LAYOUT --->