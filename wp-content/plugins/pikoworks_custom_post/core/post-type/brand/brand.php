<?php
// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}
if (!function_exists('pikoworks_create_taxonomy_brand')) {
    
    function pikoworks_create_taxonomy_brand() {
        // Taxonomy 
        $labels = array(
        	'name'              => esc_html__( 'Brands', 'pikoworks-core' ),
                'singular_name'     => esc_html__( 'Brand', 'pikoworks-core' ),
                'search_items'      => esc_html__( 'Search Brands', 'pikoworks-core' ),
                'all_items'         => esc_html__( 'All Brands', 'pikoworks-core' ),
                'parent_item'       => esc_html__( 'Parent Brand', 'pikoworks-core' ),
                'parent_item_colon' => esc_html__( 'Parent Brand:', 'pikoworks-core' ),
                'edit_item'         => esc_html__( 'Edit Brand', 'pikoworks-core' ),
                'update_item'       => esc_html__( 'Update Brand', 'pikoworks-core' ),
                'add_new_item'      => esc_html__( 'Add New Brand', 'pikoworks-core' ),
                'new_item_name'     => esc_html__( 'New Brand Name', 'pikoworks-core' ),
                'menu_name'         => esc_html__( 'Brands', 'pikoworks-core' ),
        ); 
        $args = array(
                'hierarchical'      => true,
                'labels'            => $labels,
                'show_ui'           => true,
                'show_admin_column' => true,
                'query_var'         => true,
                'capabilities'      => array(
                'manage_terms'      => 'manage_product_terms',
                        'edit_terms' 	=> 'edit_product_terms',
                        'delete_terms'  => 'delete_product_terms',
                        'assign_terms' 	=> 'assign_product_terms',
                ),
                'rewrite'           => array( 'slug' => 'brand' ),
        );
        register_taxonomy( 'brand', array( 'product' ), $args );
        flush_rewrite_rules();
    }
    add_action('init', 'pikoworks_create_taxonomy_brand');
} 
