<?php
/*
 * @author sw-theme/themepiko
 */
$prefix = 'xtocky_';
$footer_width =  get_post_meta(get_the_ID(), $prefix . 'footer_width',true);
if (!isset($footer_width) || $footer_width == '-1' || $footer_width == '') {
    $footer_width = isset( $GLOBALS['xtocky']['footer-width-content'] ) ? $GLOBALS['xtocky']['footer-width-content'] : 'container-fluid';
}

$footer_bottom_layout =  get_post_meta(get_the_ID(), $prefix . 'footer_bottom_layout',true);
if (!isset($footer_bottom_layout) || $footer_bottom_layout == '-1' || $footer_bottom_layout == '') {
    $footer_bottom_layout = isset( $GLOBALS['xtocky']['optn_footer_bottom_layout'] ) ? $GLOBALS['xtocky']['optn_footer_bottom_layout'] : '3';
}
$footer_copyright_text =  get_post_meta(get_the_ID(), $prefix . 'sub_footer_text',true);
if (!isset($footer_copyright_text) || $footer_copyright_text == '') {
    $footer_copyright_text = isset( $GLOBALS['xtocky']['sub_footer_text'] ) ? $GLOBALS['xtocky']['sub_footer_text'] : sprintf( esc_html__( 'Proudly powered by %s', 'xtocky' ), 'WordPress' );
}
$footer_payment_logo = isset( $GLOBALS['xtocky']['optn_payment_logo_upload'] ) ? $GLOBALS['xtocky']['optn_payment_logo_upload'] : '';
$footer_social_class = isset( $GLOBALS['xtocky']['footer_social'] ) ? $GLOBALS['xtocky']['footer_social'] : '';

$payment_icon_class = '';
if($footer_payment_logo != '' || $footer_social_class != ''){
   $payment_icon_class = 'payment-icon-wrap';
}
?>
<footer class="footer" style="    background-color: #2f2f2f;" id="contacto">
    <div class="padding-panel">
      <div class="container">
        <div class="row">
          <div class="col-md-3"><img class="footer-logo" src="http://vasesorias.cl/img/LogoBlanco.png" alt=""/>
            <div class="footer-text excen" style="    text-align: justify !important;">Valdevellano Asesorías tiene como misión brindar seguridad y metodologías de prevención de riesgos a nuestros colaboradores con el fin de desarrollar sus labores en un ambiente de trabajo adecudado</div>
          </div>
          <div class="col-md-2">
            <h2 class="title-footer">Menú</h2>
            <ul class="menuFooter">
              <li><a class="smooth" href="http://vasesorias.cl/tienda">Inicio</a></li>
              <li><a class="smooth" href="http://vasesorias.cl/tienda/tienda">Tienda</a></li>
              <li><a class="smooth" href="http://vasesorias.cl/tienda/mi-cuenta/">Mi cuenta</a></li>
              <!--<li><a class="smooth" href="#tienda">Tienda</a></li>-->
              <li><a class="smooth" href="http://vasesorias.cl/tienda/wishlist/">Lista de deseos</a></li>
              <li><a class="smooth" href="http://vasesorias.cl/tienda/carrito/">Carrito</a></li>
              <li><a class="smooth" href="http://vasesorias.cl/tienda/faqs/">FAQs</a></li>
              <li><a class="smooth" href="http://vasesorias.cl/tienda/acerca-de/">Acerca de</a></li>
              <li><a class="smooth" href="http://vasesorias.cl/tienda/contacto/">Contacto</a></li>
              <li><a class="smooth" href="http://vasesorias.cl">Ir a VA Asesorias</a></li>
            </ul>
          </div>
          <div class="col-md-3">
            <h2 class="title-footer">Contacto</h2>
            <div class="d-flex flex-column">
              <div class="m-r-10 footer-text d-flex align-items-center m-b-15 none-right-margin justify-content-max"><i class="ion ion-md-locate m-r-15"></i><span class="f-5" style="font-weight:500">Merced 838-A 117 Santiago</span></div>
              <div class="m-r-10 footer-text d-flex align-items-center m-b-15 none-right-margin justify-content-max"><i class="ion ion-md-mail m-r-15"></i><span class="f-5" style="font-weight:500">ventas@vasesorias.cl</span></div>
              <div class="m-r-10 footer-text d-flex align-items-center none-right-margin justify-content-max"><i class="ion ion-md-call m-r-15"></i>
                <span class="f-5" style="font-weight:500;display:block">+56 9 9577 3364</span>




              </div>
              <div class="m-r-10 m-t-10 footer-text d-flex align-items-center none-right-margin justify-content-max"><i class="ion ion-md-call m-r-15"></i>
              <span class="f-5" style="font-weight:500;display:block">+56 9 7798 3668</span>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <h2 class="title-footer">Formulario de contacto</h2>
            <div id="contact_footer">
              <div class="wpb_column vc_column_container vc_col-sm-6"><div class="vc_column-inner "><div class="wpb_wrapper"><div role="form" class="wpcf7" id="wpcf7-f17-p3787-o1" lang="es-CL" dir="ltr">
<div class="screen-reader-response"></div>
<form action="/contacto/#wpcf7-f17-p3787-o1" method="post" class="wpcf7-form" novalidate="novalidate">
<div style="display: none;">
<input type="hidden" name="_wpcf7" value="17">
<input type="hidden" name="_wpcf7_version" value="5.1.3">
<input type="hidden" name="_wpcf7_locale" value="es_CL">
<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f17-p3787-o1">
<input type="hidden" name="_wpcf7_container_post" value="3787">
</div>

    <span class="wpcf7-form-control-wrap your-name">
    <input type="text"  placeholder="Tu nombre" name="your-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false"></span>

    <span class="wpcf7-form-control-wrap your-email">
    <input type="email"  placeholder="Tu email" name="your-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false"></span>

    <span class="wpcf7-form-control-wrap your-subject">
    <input type="text"  placeholder="Tu asunto" name="your-subject" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false"></span>

    <span class="wpcf7-form-control-wrap your-message">
    <textarea name="your-message" placeholder="Tu mensaje" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false"></textarea></span>
<p><input type="submit"  value="Enviar" class="wpcf7-form-control wpcf7-submit"><span class="ajax-loader"></span></p>
<div class="wpcf7-response-output wpcf7-display-none"></div></form></div></div></div></div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="subfooter"><span class="d-flex align-items-center justify-content-center">Valdevellano Asesorías 2019. Diseñado y Desarrollado por <a href="https://www.puntodata.cl" class="d-flex align-items-center justify-content-center" target="_blank"><span><img class="svg-footer" src="http://vasesorias.cl/img/Logo%20Nuevo%20Punto%20Data.svg"></span><span class="pdata">Punto</span><span class="pdata-ye">Data SpA 2019</span></a></span></div>
    <div id="modalThree">
      <div class="modal">
        <div class="more">
          <div class="half-content is-firts">
            <div class="content-text">
              <div class="texts">
                <div class="auto-y">
                  <div class="content-description">
                    <h2 class="title-h2 title-dinami">Motto Apartments</h2>
                    <p class="item-dinami">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo fugit, ut nam fugiat id quae voluptate, maiores explicabo eaque rem accusamus asperiores repellat sed eum sint quis delectus, eligendi unde!</p>
                  </div>
                </div>
              </div>
              <div class="close-contact"><span></span><span></span></div>
            </div>
          </div>
          <div class="half-content is-second">
            <div class="content-img" style="background-image: url(img/Slider-3.png)"></div>
          </div>
        </div>
      </div>
    </div>
  </footer>
<footer id="colophon" class="footer">
    <?php xtocky_footer_sidebar_three(); ?>
    <?php xtocky_footer_sidebar_two(); ?>
    <?php xtocky_footer_sidebar_one(); ?>
    <div class="footer-bottom footer-layout-<?php echo esc_attr($footer_bottom_layout); ?>">
        <div class="<?php echo esc_attr($footer_width); ?>">
            <?php if($footer_bottom_layout == '1'): ?>
            <div class="footer-left">
                <?php
                xtocky_footer_social_icon();
                xtocky_footer_nav_menu(); ?>
            </div><!-- End .footer-right -->
            <div class="footer-right <?php echo esc_attr($payment_icon_class); ?>">
                <?php echo do_shortcode($footer_copyright_text);
                    xtocky_payment_logo();
                ?>
            </div><!-- End .footer-right -->
            <?php elseif($footer_bottom_layout == '2'): ?>
            <div class="footer-right">
                <?php
                    xtocky_footer_social_icon();
                    xtocky_payment_logo();
                    xtocky_footer_nav_menu();
                ?>
            </div><!-- End .footer-right -->
            <div class="footer-left <?php echo esc_attr($payment_icon_class); ?>">
                <?php echo do_shortcode($footer_copyright_text);?>
            </div><!-- End .footer-right -->
            <?php else: ?>

            <div class="text-center">
                <?php
                    xtocky_payment_logo();
                    echo do_shortcode($footer_copyright_text);
                    xtocky_footer_social_icon();
                ?>
            </div>
            <?php endif; //end style1 ?>
            <a class="scroll-top" href="#top" title="Scroll top"><span class="icon-arrow-long-left up"></span></a>
        </div><!-- End .container-fluid -->
    </div><!-- End .footer-bottom -->
</footer><!-- End .footer -->
