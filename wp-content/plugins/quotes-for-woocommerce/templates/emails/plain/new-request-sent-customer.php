<?php
/**
 * Request New Quote email
 */
$order_obj = new WC_order( $order->order_id );
$display_price = false;
$opening_paragraph = __( 'Has realizado una solicitud de cotización en %s. Los detalles del pedido son los siguientes:', 'quote-wc' );

do_action( 'woocommerce_email_header', $email_heading );

if( $order ) {
	echo sprintf( $opening_paragraph, $site_name );
}

if ( $order_obj ) {
    echo sprintf( __( 'Producto', 'quote-wc' ) );
    echo sprintf( __( 'Cantidad', 'quote-wc' ) );
    if( qwc_order_display_price( $order_obj ) ) {
        $display_price = true;
        echo sprintf( __( 'Precio de producto', 'quote-wc' ) );
    }

    echo "\n";

	foreach( $order_obj->get_items() as $items ) {

        echo $items->get_name();
        echo $items->get_quantity();
        if( $display_price ) {
            echo $order_obj->get_formatted_line_subtotal( $items );
        }
        echo "\n";

	}

    echo sprintf( __( 'Este pedido está a la espera de una cotización.', 'quote-wc' ) );

    echo sprintf( __( 'Usted recibirá un correo electrónico de la cita del administrador del sitio pronto.', 'quote-wc' ) );

    do_action( 'woocommerce_email_footer' );
}
