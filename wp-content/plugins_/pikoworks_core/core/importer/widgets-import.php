<?php  if ( ! defined('ABSPATH')) exit('No direct script access allowed');

/*
@Author: themepiko
@first delete old widgets then installed theme default widgets.
@ default widgets add import.php function: import_widget_all_data
*/

return array(
	'default' => array(
		'sidebar-widgets' => array(
			'sidebar-1' => array('flush' => true), //main sidebar                       
                        'sidebar-3' => array('flush' => true), //footer inner
                        'sidebar-5' => array('flush' => true), //footer innter top
                        'sidebar-4' => array('flush' => true),
			'sidebar-7' => array('flush' => true), 
			'Footer Inner v2' => array('flush' => true), //footer innter top
			'Footer Inner v3' => array('flush' => true), //footer innter top2
			'Vertical bottom widget' => array('flush' => true),
		),
		'custom-sidebars' => array( //custom sidebar
			'Footer Inner v2',
			'Footer Inner v3',
			'Vertical bottom widget',
			'blank widget'
		)
	),	
	'electronics' => array(
		'sidebar-widgets' => array(
			'sidebar-1' => array('flush' => true), //main sidebar                       
                        'sidebar-2' => array('flush' => true), //page sidebar
                        'sidebar-3' => array('flush' => true), //footer inner
                        'sidebar-4' => array('flush' => true), // shop sidebar
                        'sidebar-5' => array('flush' => true), //footer innter top                        
                        'sidebar-6' => array('flush' => true), //footer innter top                        
			'sidebar-7' => array('flush' => true), //Shop Filter Sidebar
			'sidebar-8' => array('flush' => true) //Catalog Sidebar
		),
		'custom-sidebars' => array( //custom sidebar
			'blank widget'
		)
	),
	'furniture' => array(
		'sidebar-widgets' => array(
			'sidebar-1' => array('flush' => true), //main sidebar                       
                        'sidebar-2' => array('flush' => true), //page sidebar
                        'sidebar-3' => array('flush' => true), //footer inner
                        'sidebar-4' => array('flush' => true), // shop sidebar
                        'sidebar-5' => array('flush' => true), //footer innter top                        
                        'sidebar-6' => array('flush' => true), //footer innter top                        
			'sidebar-7' => array('flush' => true), //Shop Filter Sidebar
			'sidebar-8' => array('flush' => true) //Catalog Sidebar
		),
		'custom-sidebars' => array( //custom sidebar
			'blank widget'
		)
	),
	'cosmetics' => array(
		'sidebar-widgets' => array(
			'sidebar-1' => array('flush' => true), //main sidebar                       
                        'sidebar-2' => array('flush' => true), //page sidebar
                        'sidebar-3' => array('flush' => true), //footer inner
                        'sidebar-4' => array('flush' => true), // shop sidebar
                        'sidebar-5' => array('flush' => true), //footer innter top                        
                        'sidebar-6' => array('flush' => true), //footer innter top                        
			'sidebar-7' => array('flush' => true), //Shop Filter Sidebar
			'sidebar-8' => array('flush' => true) //Catalog Sidebar
		),
		'custom-sidebars' => array( //custom sidebar
			'blank widget'
		)
	),
	'jewelry' => array(
		'sidebar-widgets' => array(
			'sidebar-1' => array('flush' => true), //main sidebar                       
                        'sidebar-2' => array('flush' => true), //page sidebar
                        'sidebar-3' => array('flush' => true), //footer inner
                        'sidebar-4' => array('flush' => true), // shop sidebar
                        'sidebar-5' => array('flush' => true), //footer innter top                        
                        'sidebar-6' => array('flush' => true), //footer innter top                        
			'sidebar-7' => array('flush' => true), //Shop Filter Sidebar
			'sidebar-8' => array('flush' => true) //Catalog Sidebar
		),
		'custom-sidebars' => array( //custom sidebar
			'blank widget'
		)
	),
        'dokan' => array(
		'sidebar-widgets' => array(
			'sidebar-1' => array('flush' => true), //main sidebar                       
                        'sidebar-2' => array('flush' => true), //page sidebar
                        'sidebar-3' => array('flush' => true), //footer inner
                        'sidebar-4' => array('flush' => true), // shop sidebar
                        'sidebar-5' => array('flush' => true), //footer innter top                        
                        'sidebar-6' => array('flush' => true), //footer innter top                        
			'sidebar-7' => array('flush' => true), //Shop Filter Sidebar
			'sidebar-8' => array('flush' => true) //Catalog Sidebar
		),
		'custom-sidebars' => array( //custom sidebar
			'blank widget'
		)
	),
        'vendor' => array(
		'sidebar-widgets' => array(
			'sidebar-1' => array('flush' => true), //main sidebar                       
                        'sidebar-2' => array('flush' => true), //page sidebar
                        'sidebar-3' => array('flush' => true), //footer inner
                        'sidebar-4' => array('flush' => true), // shop sidebar
                        'sidebar-5' => array('flush' => true), //footer innter top                        
                        'sidebar-6' => array('flush' => true), //footer innter top                        
			'sidebar-7' => array('flush' => true), //Shop Filter Sidebar
			'sidebar-8' => array('flush' => true) //Catalog Sidebar
		),
		'custom-sidebars' => array( //custom sidebar
			'blank'
		)
	),
        'rtl' => array(
		'sidebar-widgets' => array(
			'sidebar-1' => array('flush' => true), //main sidebar                       
                        'sidebar-2' => array('flush' => true), //page sidebar
                        'sidebar-3' => array('flush' => true), //footer inner
                        'sidebar-4' => array('flush' => true), // shop sidebar
                        'sidebar-5' => array('flush' => true), //footer innter top                        
                        'sidebar-6' => array('flush' => true), //footer innter top                        
			'sidebar-7' => array('flush' => true), //Shop Filter Sidebar
			'sidebar-8' => array('flush' => true) //Catalog Sidebar
		),
		'custom-sidebars' => array( //custom sidebar
			'blank'
		)
	),
	
);