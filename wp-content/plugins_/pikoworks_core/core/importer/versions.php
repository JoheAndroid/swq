<?php  if ( ! defined('ABSPATH')) exit('No direct script access allowed');

$main_domain = 'http://themepiko.com/demo/stock/';

return array(
    'default' => array(
        'title' => 'Default',
        'preview_url' => $main_domain. 'default/',
        'to_import' => array(
            'content' => true,
            'slider' => true,
            'widgets' => true,
            'options' => true,
            'menu' => true,
            'home_page' => true
        ),
        'type' => 'demo'
    ),
    'main' => array(
        'title' => 'Main demo',
        'preview_url' => $main_domain. 'default/',
        'to_import' => array(
            'content' => true,
            'home_page' => true
        ),
        'type' => 'demo'
    ),
    'electronics' => array(
        'title' => 'Electronics',
        'preview_url' => $main_domain. 'electronics/',
        'to_import' => array(
            'content' => true,
            'slider' => true,
            'widgets' => true,
            'options' => true,
            'menu' => true,
            'home_page' => true
        ),
        'type' => 'demo'
    ),
    'furniture' => array(
        'title' => 'Furniture',
        'preview_url' => $main_domain. 'furniture/',
        'to_import' => array(
            'content' => true,
            'slider' => true,
            'widgets' => true,
            'options' => true,
            'menu' => true,
            'home_page' => true
        ),
        'type' => 'demo'
    ),
    'cosmetics' => array(
        'title' => 'Cosmetics',
        'preview_url' => $main_domain. 'cosmetics/',
        'to_import' => array(
            'content' => true,
            'slider' => true,
            'widgets' => true,
            'options' => true,
            'menu' => true,
            'home_page' => true
        ),
        'type' => 'demo'
    ),
    'jewelry' => array(
        'title' => 'Jewelry',
        'preview_url' => $main_domain. 'jewelry/',
        'to_import' => array(
            'content' => true,
            'slider' => true,
            'widgets' => true,
            'options' => true,
            'menu' => true,
            'home_page' => true
        ),
        'type' => 'demo'
    ),
    'dokan' => array(
        'title' => 'Multi Vendor dokan',
        'preview_url' => $main_domain. 'dokan/',
        'to_import' => array(
            'content' => true,
            'slider' => true,
            'widgets' => true,
            'options' => true,
            'menu' => true,
            'home_page' => true
        ),
        'type' => 'demo'
    ),
    'vendor' => array(
        'title' => 'WC Vendor + buddypress',
        'preview_url' => $main_domain. 'vendor/',
        'to_import' => array(
            'content' => true,
            'slider' => true,
            'widgets' => true,
            'options' => true,
            'menu' => true,
            'home_page' => true
        ),
        'type' => 'demo'
    ),
    'rtl' => array(
        'title' => 'RTL version Arabia',
        'preview_url' => $main_domain. 'rtl/',
        'to_import' => array(
            'content' => true,
            'slider' => true,
            'widgets' => true,
            'options' => true,
            'menu' => true,
            'home_page' => true
        ),
        'type' => 'demo'
    ),
    'faq' => array(
        'title' => 'Faq',
        'preview_url' => $main_domain . 'default/faqs/',
        'to_import' => array(
            'content' => true
        ),
        'type' => 'page'
    ),
    '2' => array(
        'title' => 'Home 2',
        'preview_url' => $main_domain . 'default/home2/',
        'to_import' => array(
            'content' => true,
            'home_page' => true,
            'slider' => true,
        ),
        'type' => 'page'
    ),
    '3' => array(
        'title' => 'Home 3',
        'preview_url' => $main_domain . 'default/home3/',
        'to_import' => array(
            'content' => true,
            'home_page' => true,
            'slider' => true,
        ),
        'type' => 'page'
    ),
    '4' => array(
        'title' => 'Home 4',
        'preview_url' => $main_domain . 'default/home4/',
        'to_import' => array(
            'content' => true,
            'home_page' => true,
            'slider' => true,
        ),
        'type' => 'page'
    ),
    '5' => array(
        'title' => 'Home 5',
        'preview_url' => $main_domain . 'default/home5/',
        'to_import' => array(
            'content' => true,
            'home_page' => true,
            'slider' => true,
        ),
        'type' => 'page'
    ),
    '6' => array(
        'title' => 'Home 6',
        'preview_url' => $main_domain . 'default/home6/',
        'to_import' => array(
            'content' => true,
            'home_page' => true,
            'slider' => true,
        ),
        'type' => 'page'
    ),
    '7' => array(
        'title' => 'Home 7',
        'preview_url' => $main_domain . 'default/home7/',
        'to_import' => array(
            'content' => true,
            'home_page' => true,
            'slider' => true,
        ),
        'type' => 'page'
    ),
    '8' => array(
        'title' => 'Home 8',
        'preview_url' => $main_domain . 'default/home8/',
        'to_import' => array(
            'content' => true,
            'home_page' => true,
            'slider' => true,
        ),
        'type' => 'page'
    ),
    '9' => array(
        'title' => 'Home 9',
        'preview_url' => $main_domain . 'default/home9/',
        'to_import' => array(
            'content' => true,
            'home_page' => true,
            'slider' => true,
        ),
        'type' => 'page'
    ),
    '10' => array(
        'title' => 'Home 10',
        'preview_url' => $main_domain . 'default/home10/',
        'to_import' => array(
            'content' => true,
            'home_page' => true,
            'slider' => true,
        ),
        'type' => 'page'
    ),
    '11' => array(
        'title' => 'Home 11',
        'preview_url' => $main_domain . 'default/home11/',
        'to_import' => array(
            'content' => true,
            'home_page' => true,
            'slider' => true,
        ),
        'type' => 'page'
    ),
    '12' => array(
        'title' => 'Home 12',
        'preview_url' => $main_domain . 'default/home12/',
        'to_import' => array(
            'content' => true,
            'home_page' => true,
            'slider' => true,
        ),
        'type' => 'page'
    ),
    'about_us' => array(
        'title' => 'about us',
        'preview_url' => $main_domain . 'default/about-us/',
        'to_import' => array(
            'content' => true
        ),
        'type' => 'page'
    ),
    'about_me' => array(
        'title' => 'about me',
        'preview_url' => $main_domain . 'default/about-me/',
        'to_import' => array(
            'content' => true
        ),
        'type' => 'page'
    ),
    'team' => array(
        'title' => 'team members',
        'preview_url' => $main_domain . 'default/team-layout/',
        'to_import' => array(
            'content' => true
        ),
        'type' => 'page'
    ),
    'portfolios' => array(
        'title' => 'Portfolios',
        'preview_url' => $main_domain . 'default/portfolios/portfolio-grid-4col/',
        'to_import' => array(
            'content' => true
        ),
        'type' => 'page'
    ),
    'testimonial' => array(
        'title' => 'testimonial',
        'preview_url' => $main_domain . 'default/testimonial-layout/',
        'to_import' => array(
            'content' => true
        ),
        'type' => 'page'
    ),
    'vendor_home' => array(
        'title' => 'Vendor Homepage',
        'preview_url' => $main_domain . 'vendor/',
        'to_import' => array(
            'content' => true,
            'home_page' => true,
            'slider' => true
        ),
        'type' => 'page'
    )
);