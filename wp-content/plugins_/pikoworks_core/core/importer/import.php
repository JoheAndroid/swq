<?php 

if(!class_exists('pikoworks_import_data')) {

	class pikoworks_import_data {

		private $_import_url = '';

		private $_widgets_counter = 0;

		private $_folder = '';

		private $_remote_folder = '';

		private $_version = '';

		private $_content = 'full';

		private $_all_widgets = array();

		public $versions = array();

		public function __construct() {
			add_action( 'init', array( $this, 'init' ) );                        
		}

		public function init() {                       
                    
			if( ! defined( 'XTOCKY_THEME_SLUG' ) ) return;
			$this->_import_url 	= 'http://themepiko.com/import/' . XTOCKY_THEME_SLUG . '_versions/';
			$this->_all_widgets 	= require PIKOWORKSCORE_CORE . 'importer/widgets-import.php';
			$this->versions 	= require  PIKOWORKSCORE_CORE . 'importer/versions.php';                       
			add_action('wp_ajax_pikoworks_import_ajax', array($this, 'import_data'));
                        
		}

		public function import_data() {
			//delete_option('demo_data_installed');die();
			//sleep(3); echo 'test complete'; die();
			$versions_imported = get_option('versions_imported');

			if( empty( $versions_imported ) ) $versions_imported = array();

			$xml_result = '';

			if(!empty($_POST['version'])) {
				$this->_version = $_POST['version'];
			}

			if( ! empty( $this->versions[ $this->_version ] ) ) {

				

				$to_import = $this->versions[ $this->_version ]['to_import'];

				$this->_folder = PIKOWORKSCORE_CORE . 'importer/dummy/' . $this->_version . '/';

				$this->_remote_folder = PIKOWORKSCORE_CORE_URL . 'importer/dummy/' . $this->_version . '/';

				if( ! empty( $to_import['content'] ) && ! in_array( $this->_version, $versions_imported ) ) {
					$xml_result = $this->import_xml_file();
				}

				if( ! empty( $to_import['slider'] ) ) {
					for( $i = 0; $i < $to_import['slider']; $i++ ) {
						$slider_result = $this->import_slider( $i );
					}
				}

				if( ! empty( $to_import['menu'] ) && ! in_array( $this->_version, $versions_imported ) ) {
					$this->update_menus();
				}

				if( ! empty( $to_import['widgets'] ) ) {
					$this->update_widgets();
                                        $this->import_widget_all_data(); //multi
				}                             

				if( ! empty( $to_import['home_page'] ) ) {
					$this->update_home_page();
				}

				if( ! empty( $to_import['options'] ) ) {
					$this->update_options();
				}
			}

			

			echo '<p><strong>Successfully imported!</strong></p>';

			if($xml_result) {
				echo $xml_result;
			} else {
				//echo '<p>XML not imported.</p>';
			}

			if(isset($slider_result['success']) && $slider_result['success'] != '') {
				echo '<p>Revolutions slider has been successfully imported!</p>';
			}

			$versions_imported[] = $this->_version;

			update_option('versions_imported', $versions_imported);

			die();
		}

		public function import_slider_from_url() {

			$folder = $this->_import_url . $this->_version;
			$sliderZip = $folder . '/slider.zip';
			$slider_data = wp_remote_get($sliderZip);

			if( ! is_wp_error($slider_data) ) {

				$tmpZip = XTOCKY_THEME_DIR.'inc/tmp/tempSliderZip.zip';
				file_put_contents($tmpZip, $slider_data['body']);
				return $this->import_slider();
			}
		}

		public function import_slider( $i = 0 ) {

			$zip_file = ( $i > 0 ) ? $this->_folder . 'slider' . $i . '.zip' : $this->_folder . 'slider.zip' ;

			if(!class_exists('RevSlider')) return;

			$revapi = new RevSlider();

			ob_start();

			$slider_result = $revapi->importSliderFromPost(true, true, $zip_file);

			ob_end_clean();

			return $slider_result;

		}

		public function import_xml_from_url() {
			$folder = $this->_import_url . $this->_version;

			$version_xml = $folder.'/content-' . $this->_content . '.xml';

			$version_data = wp_remote_get($version_xml);

			if( ! is_wp_error($version_data)) {

				$tmpxml = XTOCKY_THEME_DIR.'inc/tmp/version_data.xml';

				file_put_contents($tmpxml, $version_data['body']);

				return $this->import_xml_file();

			}

			return false;
		}

		public function import_xml_file() {

			$result = false;

			// Load Importer API
			require_once ABSPATH . 'wp-admin/includes/import.php';

			$importerError = false;

			//check if wp_importer, the base importer class is available, otherwise include it
			if ( !class_exists( 'WP_Importer' ) ) {
				$class_wp_importer = ABSPATH . 'wp-admin/includes/class-wp-importer.php';
				if ( file_exists( $class_wp_importer ) )
					require_once($class_wp_importer);
				else
					$importerError = true;
			}


			if($importerError !== false) {
				echo ("The Auto importing script could not be loaded. Please use the wordpress importer and import the XML file that is located in your themes folder manually.");
				return;
			}


			if(class_exists('WP_Importer')) {

				try {

					ob_start();

					add_filter( 'intermediate_image_sizes', array( $this, 'sizes_array') );

					$file_url = $this->_folder . 'dummy.xml';

					$importer = new WP_Import();

					$importer->fetch_attachments = true;

					$importer->import($file_url);

					$result = ob_get_clean();

				} catch (Exception $e) {
					$result = false;
					echo ("Error while importing");
				}

			}

			return $result;

		}

		public function sizes_array( $sizes ) {
			return array();
		}

		/**
		 *
		 */
		public function update_menus(){

			global $wpdb;

			$menu_name = 'Main menu';
			$primay_menu_location = 'primary';
			$top_menu_location = 'top_menu';
			$footer_menu_location = 'footer';

			$tablename = $wpdb->prefix.'terms';
			$menu_ids = $wpdb->get_results(
				"
			    SELECT term_id
			    FROM ".$tablename."
			    WHERE name= '".$menu_name."'
			    "
			);

			// results in array
			foreach($menu_ids as $menu):
				$menu_id = $menu->term_id;
			endforeach;

			$shop_page = get_page_by_title('Shop');

			$itemData =  array(
				'menu-item-object-id'	=> $shop_page->ID,
				'menu-item-parent-id'	=> 0,
				'menu-item-position'  	=> 2,
				'menu-item-object' 		=> 'page',
				'menu-item-type'      	=> 'post_type',
				'menu-item-status'    	=> 'publish'
			);

			wp_update_nav_menu_item($menu_id, 0, $itemData);

			if( !has_nav_menu( $primay_menu_location ) ){
				$locations = get_theme_mod('nav_menu_locations');
				$locations[$primay_menu_location] = $menu_id;
				set_theme_mod( 'nav_menu_locations', $locations );
			}

		}

		private function update_widgets() {

			$widgets = $this->_all_widgets[ $this->_version ];

			// We don't want to undo user changes, so we look for changes first.
			$this->_active_widgets = get_option( 'sidebars_widgets' );

			$this->_widgets_counter = 1;

			if( ! empty( $widgets['custom-sidebars'] ) ) {
				foreach ($widgets['custom-sidebars'] as $customsidebar) {
					xtocky_add_sidebar( $customsidebar );
				}
			}

			foreach ($widgets['sidebar-widgets'] as $area => $params) {
				if ( ! empty ( $this->_active_widgets[$area] ) && $params['flush'] ) {
					$this->_flush_widget_area($area);
				} else if(! empty ( $this->_active_widgets[$area] ) && ! $params['flush'] ) {
					continue;
				}
				foreach ($params['widgets'] as $widget => $args) {
					$this->_add_widget($area, $widget, $args);
				}
			}
			// Now save the $active_widgets array.
			update_option( 'sidebars_widgets', $this->_active_widgets );

		}

		private function _add_widget( $sidebar, $widget, $options = array() ) {
			$this->_active_widgets[ $sidebar ][] = $widget . '-' . $this->_widgets_counter;
			$widget_content = get_option( 'widget_' . $widget );
			$widget_content[ $this->_widgets_counter ] = $options;
			update_option(  'widget_' . $widget, $widget_content );
			$this->_widgets_counter++;
		}

		private function _flush_widget_area( $area ) {
			unset($this->_active_widgets[ $area ]);
		}
                
                public function import_widget_all_data() {
                    
                      $version = $this->_version;
                      $data = '';
                       if($version == 'default'){
                           $data = '{"sidebar-1":{"recent_postimage-4":{"title":"Latest Posts","show":"3"},"categories-1":{"title":"Categories","count":0,"hierarchical":0,"dropdown":0},"tag_cloud-13":{"title":false,"taxonomy":"post_tag"},"null-instagram-feed-4":{"title":"Instagram","username":"piko.stock","number":"6","columns":3,"size":"thumbnail","target":"_blank","link":"","slider":false,"spacing":false},"piko-socials-icon-5":{"title":"Socials"}},"sidebar-3":{"custom_html-11":{"title":"","content":"<img  src=\"http:\/\/themepiko.com\/demo\/stock\/default\/wp-content\/uploads\/2016\/12\/logo.png\" alt=\"\"\/>\r\n<p style=\"padding-right:30px\">\r\nThis is easy to update text from footer widget area. Add here information about your store.<\/p>\r\n<p>    25 Southern Avenue, USA.<br>\r\n<abbr title=\"Phone Number\"><strong>Phone:<\/strong><\/abbr> (123) 4567 890<br>\r\n<abbr title=\"Fax\"><strong>Fax:<\/strong><\/abbr> (123) 4567 890 22<br>\r\n<abbr title=\"Email Address\"><strong>Email:<\/strong><\/abbr> info@company.com<br>\r\n<\/p>\r\n"},"custom_html-12":{"title":"Categories","content":"<ul class=\"links\">\r\n<li><a href=\"#\">Men<\/a><\/li>\r\n<li><a href=\"#\">Women<\/a><\/li>\r\n<li><a href=\"#\">Shoes<\/a><\/li>\r\n<li><a href=\"#\">Denim<\/a><\/li>\r\n<li><a href=\"#\">Accessories<\/a><\/li>\r\n<li><a href=\"#\">Jackets<\/a><\/li>\r\n<\/ul>"},"custom_html-13":{"title":"Quick Links","content":"<ul class=\"links\">\r\n<li><a href=\"#\">Store Location<\/a><\/li>\r\n<li><a href=\"#\">My Account<\/a><\/li>\r\n<li><a href=\"#\">Orders Tracking<\/a><\/li>\r\n<li><a href=\"#\">Careers<\/a><\/li>\r\n<li><a href=\"#\">FAQs<\/a><\/li>\r\n<li><a href=\"#\">Legal Notice<\/a><\/li>\r\n<\/ul>"},"custom_html-14":{"title":"Information","content":"<ul class=\"links\">\r\n<li><a href=\"#\">About Us<\/a><\/li>\r\n<li><a href=\"#\">Contact Us<\/a><\/li>\r\n<li><a href=\"#\">Terms & Conditions<\/a><\/li>\r\n<li><a href=\"#\">Returns & Exchanges<\/a><\/li>\r\n<li><a href=\"#\">Privacy Policy<\/a><\/li>\r\n<li><a href=\"#\">Shipping & Delivery<\/a><\/li>\r\n<\/ul>"},"custom_html-15":{"title":"","content":"Subscribe to our newsletter and get 15% off your first three Product<p><\/p>\r\n\r\n[contact-form-7 id=\"4439\" title=\"Subscribe Form\"]\r\n<p><\/p>\r\n<img  src=\"http:\/\/themepiko.com\/demo\/stock\/default\/wp-content\/uploads\/2017\/04\/payment.png\" \/>"}},"sidebar-5":{"woocommerce_top_rated_products-5":{"title":"Top Rated","number":3},"woocommerce_products-4":{"title":"On Sale","number":3,"show":"onsale","orderby":"rand","order":"desc","hide_free":0,"show_hidden":0},"woocommerce_products-5":{"title":"Top Sellers","number":3,"show":"","orderby":"sales","order":"desc","hide_free":0,"show_hidden":0},"woocommerce_products-6":{"title":"Recomended","number":3,"show":"featured","orderby":"rand","order":"desc","hide_free":0,"show_hidden":0}},"sidebar-6":{"latest_tweets_widget-8":{"title":"Latest Tweets","screen_name":"seeyou38","num":"5","pop":"0"},"piko_flickr_widget-3":{"title":"Flickr Stream","screen_name":"48126477@N05","number":"6"},"null-instagram-feed-5":{"title":"Instagram","username":"piko.stock","number":"6","columns":"2","size":"thumbnail","target":"_blank","link":"Follow Us","slider":false,"spacing":true},"tag_cloud-14":{"title":"Product Categories","taxonomy":"product_cat"},"nav_menu-3":{"nav_menu":56}},"sidebar-4":{"pikoworks_widget_brands-6":{"title":"Filter by Brand","displayType":"image","dropdown":0,"count":1,"hide_empty":0},"woocommerce_price_filter-5":{"title":"Filter by price"},"woocommerce_product_categories-5":{"title":"Product categories","orderby":"name","dropdown":0,"count":0,"hierarchical":1,"show_children_only":0,"hide_empty":0},"woocommerce_top_rated_products-6":{"title":"Top Rated","number":3},"tag_cloud-15":{"title":false,"taxonomy":"product_tag"}},"sidebar-7":{"pikoworks_widget_brands-7":{"title":"Filter by brand","displayType":"name","dropdown":0,"count":1,"hide_empty":0},"tag_cloud-16":{"title":"Color Filter","taxonomy":"pa_color"},"tag_cloud-17":{"title":"Size Filter","taxonomy":"pa_size"},"woocommerce_price_filter-6":{"title":"Filter by price"},"piko-socials-icon-6":{"title":"Socials"}},"sidebar-8":{"woocommerce_product_categories-6":{"title":"","orderby":"name","dropdown":0,"count":0,"hierarchical":0,"show_children_only":1,"hide_empty":0}},"verticalbottomwidget":{"piko-socials-icon-7":{"title":""},"pikoworks_widget_brands-8":{"title":"Filter by brand","displayType":"image","dropdown":0,"count":1,"hide_empty":1}},"footerinnerv2":{"custom_html-16":{"title":"","content":"<img  src=\"http:\/\/themepiko.com\/demo\/stock\/default\/wp-content\/uploads\/2016\/12\/logo.png\" alt=\"\"\/>\r\n<p style=\"padding-right:30px\">\r\nThis is easy to update text from footer widget area. Add here information about your store.<\/p>\r\n<p>    25 Southern Avenue, USA.<br>\r\n<abbr title=\"Phone Number\"><strong>Phone:<\/strong><\/abbr> (123) 4567 890<br>\r\n<abbr title=\"Fax\"><strong>Fax:<\/strong><\/abbr> (123) 4567 890 22<br>\r\n<abbr title=\"Email Address\"><strong>Email:<\/strong><\/abbr> info@company.com<br>\r\n<\/p>\r\n"},"tag_cloud-18":{"title":"Categories","taxonomy":"product_cat"},"custom_html-17":{"title":"Information","content":"<ul class=\"links\">\r\n<li><a href=\"#\">Store Location<\/a><\/li>\r\n<li><a href=\"#\">My Account<\/a><\/li>\r\n<li><a href=\"#\">Orders Tracking<\/a><\/li>\r\n<li><a href=\"#\">Careers<\/a><\/li>\r\n<li><a href=\"#\">FAQs<\/a><\/li>\r\n<li><a href=\"#\">Legal Notice<\/a><\/li>\r\n<\/ul>"},"text-36":{"title":"","text":"[piko_testimonial type=\"3\" number=\"3\" excerpt=\"20\" is_slider=\"yes\" navigation=\"false\" dots=\"true\" use_responsive=\"0\"]","filter":false}},"footerinnerv3":{"custom_html-18":{"title":"","content":"<img  src=\"http:\/\/themepiko.com\/demo\/stock\/default\/wp-content\/uploads\/2016\/12\/logo.png\" alt=\"\"\/>\r\n<p style=\"padding-right:30px\">\r\nThis is easy to update text from footer widget area. Add here information about your store.<\/p>\r\n<p>    25 Southern Avenue, USA.<br>\r\n<abbr title=\"Phone Number\"><strong>Phone:<\/strong><\/abbr> (123) 4567 890<br>\r\n<abbr title=\"Fax\"><strong>Fax:<\/strong><\/abbr> (123) 4567 890 22<br>\r\n<abbr title=\"Email Address\"><strong>Email:<\/strong><\/abbr> info@company.com<br>\r\n<\/p>\r\n"},"custom_html-19":{"title":"Information","content":"<ul class=\"links\">\r\n<li><a href=\"#\">Store Location<\/a><\/li>\r\n<li><a href=\"#\">My Account<\/a><\/li>\r\n<li><a href=\"#\">Orders Tracking<\/a><\/li>\r\n<li><a href=\"#\">Careers<\/a><\/li>\r\n<li><a href=\"#\">FAQs<\/a><\/li>\r\n<li><a href=\"#\">Legal Notice<\/a><\/li>\r\n<\/ul>"},"text-39":{"title":"Testimonial","text":"[piko_testimonial type=\"3\" number=\"3\" excerpt=\"20\" is_slider=\"yes\" navigation=\"false\" dots=\"true\" use_responsive=\"0\"]","filter":false},"tag_cloud-19":{"title":"Product Category","taxonomy":"product_cat"},"latest_tweets_widget-9":{"title":"Latest Tweets","screen_name":"seeyou38","num":"5","pop":"0"}}}';
                       } elseif($version == 'electronics'){                       
                           $data = '{"sidebar-1":{"recent_postimage-1":{"title":"Latest Posts","show":"3"},"categories-3":{"title":"Categories","count":0,"hierarchical":0,"dropdown":0},"tag_cloud-1":{"title":false,"taxonomy":"post_tag"},"null-instagram-feed-1":{"title":"Instagram","username":"piko.stock","number":"6","columns":3,"size":"thumbnail","target":"_blank","link":"","slider":false,"spacing":false},"piko-socials-icon-1":{"title":"Socials"}},"sidebar-3":{"custom_html-1":{"title":"","content":"<img  src=\"http:\/\/themepiko.com\/demo\/stock\/default\/wp-content\/uploads\/2016\/12\/logo.png\" \/>\r\n<p style=\"padding-right:30px\">\r\nThis is easy to update text from footer widget area. Add here information about your store.<\/p>\r\n<p>    25 Southern Avenue, USA.<br>\r\n<abbr title=\"Phone Number\"><strong>Phone:<\/strong><\/abbr> (123) 4567 890<br>\r\n<abbr title=\"Fax\"><strong>Fax:<\/strong><\/abbr> (123) 4567 890 22<br>\r\n<abbr title=\"Email Address\"><strong>Email:<\/strong><\/abbr> info@company.com<br>\r\n<\/p>\r\n"},"custom_html-2":{"title":"Categories","content":"<ul class=\"links\">\r\n<li><a href=\"#\">Computers & Laptops<\/a><\/li>\r\n<li><a href=\"#\">Photography & Cameras<\/a><\/li>\r\n<li><a href=\"#\">Smart Phones & Tablets<\/a><\/li>\r\n<li><a href=\"#\">TV & Audio<\/a><\/li>\r\n<li><a href=\"#\">Gadgets<\/a><\/li>\r\n<li><a href=\"#\">Software<\/a><\/li>\r\n<\/ul>"},"custom_html-3":{"title":"Quick Links","content":"<ul class=\"links\">\r\n<li><a href=\"#\">Store Location<\/a><\/li>\r\n<li><a href=\"#\">My Account<\/a><\/li>\r\n<li><a href=\"#\">Orders Tracking<\/a><\/li>\r\n<li><a href=\"#\">Careers<\/a><\/li>\r\n<li><a href=\"#\">FAQs<\/a><\/li>\r\n<li><a href=\"#\">Legal Notice<\/a><\/li>\r\n<\/ul>"},"custom_html-4":{"title":"Information","content":"<ul class=\"links\">\r\n<li><a href=\"#\">About Us<\/a><\/li>\r\n<li><a href=\"#\">Contact Us<\/a><\/li>\r\n<li><a href=\"#\">Terms & Conditions<\/a><\/li>\r\n<li><a href=\"#\">Returns & Exchanges<\/a><\/li>\r\n<li><a href=\"#\">Privacy Policy<\/a><\/li>\r\n<li><a href=\"#\">Shipping & Delivery<\/a><\/li>\r\n<\/ul>"}},"sidebar-5":{"woocommerce_top_rated_products-1":{"title":"Top Rated","number":3},"woocommerce_products-1":{"title":"On Sale","number":3,"show":"onsale","orderby":"rand","order":"desc","hide_free":0,"show_hidden":0},"woocommerce_products-2":{"title":"Top Sellers","number":3,"show":"","orderby":"sales","order":"desc","hide_free":0,"show_hidden":0},"woocommerce_products-3":{"title":"Recomended","number":3,"show":"featured","orderby":"rand","order":"desc","hide_free":0,"show_hidden":0}},"sidebar-6":{"latest_tweets_widget-1":{"title":"Latest Tweets","screen_name":"seeyou38","num":"5","pop":"0"},"piko_flickr_widget-1":{"title":"Flickr Stream","screen_name":"48126477@N05","number":"6"},"null-instagram-feed-2":{"title":"Instagram","username":"piko.stock","number":"6","columns":"2","size":"thumbnail","target":"_blank","link":"Follow Us","slider":false,"spacing":true},"tag_cloud-2":{"title":"Product Categories","taxonomy":"product_cat"},"nav_menu-1":{"nav_menu":56}},"sidebar-4":{"pikoworks_widget_brands-1":{"title":"Filter by Brand","displayType":"name","dropdown":0,"count":1,"hide_empty":1},"woocommerce_price_filter-1":{"title":"Filter by price"},"woocommerce_product_categories-1":{"title":"Product categories","orderby":"name","dropdown":0,"count":0,"hierarchical":1,"show_children_only":0,"hide_empty":1},"woocommerce_top_rated_products-2":{"title":"Top Rated","number":3},"tag_cloud-3":{"title":false,"taxonomy":"product_tag"}},"sidebar-7":{"pikoworks_widget_brands-2":{"title":"Filter by brand","displayType":"name","dropdown":0,"count":1,"hide_empty":1},"woocommerce_price_filter-3":{"title":"Filter by price"},"piko-socials-icon-2":{"title":"Socials"}},"sidebar-8":{"woocommerce_product_categories-2":{"title":"","orderby":"name","dropdown":0,"count":0,"hierarchical":0,"show_children_only":1,"hide_empty":0}}}';
                       } elseif($version == 'furniture'){ 
                           $data = '{"sidebar-1":{"search-2":{"title":""},"recent-posts-2":{"title":"","number":5},"recent-comments-2":{"title":"","number":5},"archives-2":{"title":"","count":0,"dropdown":0},"categories-2":{"title":"","count":0,"hierarchical":0,"dropdown":0},"meta-2":{"title":""},"categories-3":{"title":"Categories","count":0,"hierarchical":0,"dropdown":0},"tag_cloud-1":{"title":false,"taxonomy":"post_tag"}},"sidebar-3":{"custom_html-1":{"title":"","content":"<img  src=\"http:\/\/themepiko.com\/demo\/stock\/default\/wp-content\/uploads\/2016\/12\/logo.png\" \/>\r\n<p style=\"padding-right:30px\">\r\nThis is easy to update text from footer widget area. Add here information about your store.<\/p>\r\n<p>    25 Southern Avenue, USA.<br>\r\n<abbr title=\"Phone Number\"><strong>Phone:<\/strong><\/abbr> (123) 4567 890<br>\r\n<abbr title=\"Fax\"><strong>Fax:<\/strong><\/abbr> (123) 4567 890 22<br>\r\n<abbr title=\"Email Address\"><strong>Email:<\/strong><\/abbr> info@company.com<br>\r\n<\/p>\r\n"},"custom_html-2":{"title":"Categories","content":"<ul class=\"links\">\r\n<li><a href=\"#\">Computers & Laptops<\/a><\/li>\r\n<li><a href=\"#\">Photography & Cameras<\/a><\/li>\r\n<li><a href=\"#\">Smart Phones & Tablets<\/a><\/li>\r\n<li><a href=\"#\">TV & Audio<\/a><\/li>\r\n<li><a href=\"#\">Gadgets<\/a><\/li>\r\n<li><a href=\"#\">Software<\/a><\/li>\r\n<\/ul>"},"custom_html-3":{"title":"Quick Links","content":"<ul class=\"links\">\r\n<li><a href=\"#\">Store Location<\/a><\/li>\r\n<li><a href=\"#\">My Account<\/a><\/li>\r\n<li><a href=\"#\">Orders Tracking<\/a><\/li>\r\n<li><a href=\"#\">Careers<\/a><\/li>\r\n<li><a href=\"#\">FAQs<\/a><\/li>\r\n<li><a href=\"#\">Legal Notice<\/a><\/li>\r\n<\/ul>"},"custom_html-8":{"title":"","content":"Subscribe to our newsletter and get 15% off your first three Product<p><\/p>\r\n\r\n[contact-form-7 id=\"4439\" title=\"Subscribe Form\"]\r\n<p><\/p>\r\n<img  src=\"http:\/\/themepiko.com\/demo\/stock\/default\/wp-content\/uploads\/2017\/04\/payment.png\" alt=\"\"\/>"}},"sidebar-5":{"custom_html-5":{"title":"","content":"<img  src=\"http:\/\/themepiko.com\/demo\/stock\/default\/wp-content\/uploads\/2016\/12\/logo.png\" alt=\"\"\/>\r\n<p style=\"padding-right:30px\">\r\nThis is easy to update text from footer widget area. Add here information about your store.<\/p>\r\n<p>    25 Southern Avenue, USA.<br>\r\n<abbr title=\"Phone Number\"><strong>Phone:<\/strong><\/abbr> (123) 4567 890<br>\r\n<abbr title=\"Fax\"><strong>Fax:<\/strong><\/abbr> (123) 4567 890 22<br>\r\n<abbr title=\"Email Address\"><strong>Email:<\/strong><\/abbr> info@company.com<br>\r\n<\/p>\r\n"},"custom_html-6":{"title":"Quick Link","content":"<ul class=\"links\">\r\n<li><a href=\"#\">Store Location<\/a><\/li>\r\n<li><a href=\"#\">My Account<\/a><\/li>\r\n<li><a href=\"#\">Orders Tracking<\/a><\/li>\r\n<li><a href=\"#\">Careers<\/a><\/li>\r\n<li><a href=\"#\">FAQs<\/a><\/li>\r\n<li><a href=\"#\">Legal Notice<\/a><\/li>\r\n<\/ul>"},"custom_html-7":{"title":"Information","content":"<ul class=\"links\">\r\n<li><a href=\"#\">About Us<\/a><\/li>\r\n<li><a href=\"#\">Contact Us<\/a><\/li>\r\n<li><a href=\"#\">Terms & Conditions<\/a><\/li>\r\n<li><a href=\"#\">Returns & Exchanges<\/a><\/li>\r\n<li><a href=\"#\">Privacy Policy<\/a><\/li>\r\n<li><a href=\"#\">Shipping & Delivery<\/a><\/li>\r\n<\/ul>"},"custom_html-4":{"title":"Information","content":"<ul class=\"links\">\r\n<li><a href=\"#\">About Us<\/a><\/li>\r\n<li><a href=\"#\">Contact Us<\/a><\/li>\r\n<li><a href=\"#\">Terms & Conditions<\/a><\/li>\r\n<li><a href=\"#\">Returns & Exchanges<\/a><\/li>\r\n<li><a href=\"#\">Privacy Policy<\/a><\/li>\r\n<li><a href=\"#\">Shipping & Delivery<\/a><\/li>\r\n<\/ul>"}},"sidebar-6":{"woocommerce_products-7":{"title":"New Product","number":3,"show":"","orderby":"date","order":"desc","hide_free":0,"show_hidden":0},"woocommerce_products-6":{"title":"Featured Product","number":3,"show":"featured","orderby":"date","order":"desc","hide_free":0,"show_hidden":0},"woocommerce_products-4":{"title":"Top Sellers","number":3,"show":"","orderby":"sales","order":"desc","hide_free":0,"show_hidden":0},"woocommerce_products-3":{"title":"On Sale","number":3,"show":"onsale","orderby":"rand","order":"desc","hide_free":0,"show_hidden":0}},"sidebar-4":{"woocommerce_price_filter-3":{"title":"Filter by price"},"woocommerce_product_categories-3":{"title":"Product categories","orderby":"name","dropdown":0,"count":0,"hierarchical":1,"show_children_only":0,"hide_empty":1},"woocommerce_top_rated_products-2":{"title":"Top Rated","number":3},"woocommerce_products-5":{"title":"Recomended","number":3,"show":"featured","orderby":"rand","order":"desc","hide_free":0,"show_hidden":0},"piko-socials-icon-2":{"title":"Socials"}},"sidebar-7":{"tag_cloud-3":{"title":"","count":0,"taxonomy":"product_cat"},"woocommerce_price_filter-4":{"title":"Filter by price"},"piko-socials-icon-3":{"title":"Socials"}},"sidebar-8":{"woocommerce_product_categories-4":{"title":"","orderby":"name","dropdown":0,"count":0,"hierarchical":0,"show_children_only":1,"hide_empty":0}}}'; 
                       } elseif($version == 'cosmetics'){ 
                           $data = '{"sidebar-1":{"search-2":{"title":""},"categories-2":{"title":"","count":0,"hierarchical":0,"dropdown":0},"recent_postimage-4":{"title":"Latest Posts","show":"3"},"tag_cloud-1":{"title":false,"taxonomy":"post_tag"},"null-instagram-feed-5":{"title":"Instagram","username":"cosmetics","number":"6","columns":3,"size":"thumbnail","target":"_blank","link":"","slider":false,"spacing":false},"piko-socials-icon-1":{"title":"Socials"}},"sidebar-3":{"custom_html-3":{"title":"HELPFUL LINKS","content":"<div class=\"row\">\r\n<div class=\"col-md-6 col-sm-6\">\r\n<ul class=\"links\">\r\n<li><a href=\"#\">About Us<\/a><\/li>\r\n<li><a href=\"#\">Help \/ FAQ<\/a><\/li>\r\n<li><a href=\"#\">Orders Tracking<\/a><\/li>\r\n<li><a href=\"#\">Privacy Policy <\/a><\/li>\r\n<li><a href=\"#\">Terms &amp; Conditions<\/a><\/li>\r\n<\/ul>\r\n<\/div>\r\n<div class=\"col-md-6 col-sm-6\">\r\n<ul class=\"links\">\r\n<li><a href=\"#\">Terms & Conditions<\/a><\/li>\r\n<li><a href=\"#\">Order Status<\/a><\/li>\r\n<li><a href=\"#\">Shipping Returns<\/a><\/li>\r\n<li><a href=\"#\">Contact Us<\/a><\/li>\r\n<li><a href=\"#\">Careers<\/a><\/li>\r\n<\/ul>\r\n<\/div>\r\n<\/div>"},"custom_html-5":{"title":"SIGN UP TO OUR NEWSLETTER","content":"<p style=\"padding-right:30px\">Subscribe to our newsletter and get 15% off your first three order<\/p>\r\n\r\n[contact-form-7 id=\"4439\" title=\"Subscribe Form\"]\r\n<p><\/p>\r\n<img  src=\"http:\/\/themepiko.com\/demo\/stock\/default\/wp-content\/uploads\/2017\/04\/payment.png\" \/>"},"custom_html-1":{"title":"CONTACT US","content":"<p style=\"padding-right:30px\">\r\nThis is easy to update text from footer widget area. Add here information about your store.<\/p>\r\n25 Southern Avenue, USA.<br>\r\n<abbr title=\"Phone Number\"><strong>Phone:<\/strong><\/abbr> (123) 4567 890<br>\r\n<abbr title=\"Email Address\"><strong>Email:<\/strong><\/abbr> info@company.com<br>\r\n<abbr title=\"Fax\"><strong>Fax:<\/strong><\/abbr> (123) 4567 890 22\r\n\r\n"}},"sidebar-5":{"woocommerce_top_rated_products-3":{"title":"TopRated Product","number":3},"woocommerce_products-1":{"title":"OnSale Product","number":3,"show":"onsale","orderby":"rand","order":"desc","hide_free":0,"show_hidden":0},"woocommerce_products-2":{"title":"TopSellers Product","number":3,"show":"","orderby":"sales","order":"desc","hide_free":0,"show_hidden":0},"woocommerce_products-3":{"title":"Recommend Product","number":3,"show":"featured","orderby":"rand","order":"desc","hide_free":0,"show_hidden":0}},"sidebar-6":{"woocommerce_price_filter-2":{"title":"Filter by price"},"woocommerce_product_categories-2":{"title":"Product Categories","orderby":"name","dropdown":0,"count":0,"hierarchical":1,"show_children_only":0,"hide_empty":0},"woocommerce_top_rated_products-2":{"title":"Top rated products","number":3},"latest_tweets_widget-1":{"title":"Latest Tweets","screen_name":"seeyou38","num":"5","pop":"0"},"piko_flickr_widget-1":{"title":"Flickr Stream","screen_name":"48126477@N05","number":"6"},"null-instagram-feed-6":{"title":"Instagram","username":"piko.stock","number":"6","columns":"2","size":"thumbnail","target":"_blank","link":"Follow Us","slider":false,"spacing":true},"tag_cloud-2":{"title":"Product Categories","taxonomy":"product_cat"},"nav_menu-1":{"nav_menu":56}},"sidebar-4":{"woocommerce_price_filter-3":{"title":"Filter by price"},"woocommerce_product_categories-3":{"title":"Product categories","orderby":"name","dropdown":0,"count":0,"hierarchical":1,"show_children_only":0,"hide_empty":0},"woocommerce_top_rated_products-4":{"title":"Top Rated","number":3},"tag_cloud-3":{"title":false,"taxonomy":"product_tag"}},"sidebar-7":{"tag_cloud-4":{"title":"","count":0,"taxonomy":"product_cat"},"woocommerce_price_filter-4":{"title":"Filter by price"},"piko-socials-icon-2":{"title":"Socials"}},"sidebar-8":{"woocommerce_product_categories-4":{"title":"","orderby":"name","dropdown":0,"count":0,"hierarchical":0,"show_children_only":1,"hide_empty":0}}}';
                       }elseif($version == 'jewelry'){ 
                           $data = '{"sidebar-1":{"search-2":{"title":""},"recent_postimage-2":{"title":"Latest Posts","show":"3"},"null-instagram-feed-2":{"title":"Instagram Feed","username":"tousjewelry","number":"9","columns":3,"size":"thumbnail","target":"_self","link":"Follow Us","slider":false,"spacing":false},"woocommerce_recently_viewed_products-2":{"title":"Recently Viewed Products","number":5}},"sidebar-2":{"nav_menu-2":{"nav_menu":36},"woocommerce_recently_viewed_products-3":{"title":"Recently Viewed Products","number":5}},"sidebar-3":{"custom_html-2":{"title":"CUSTOMER SERVICES","content":"<ul class=\"links\">\r\n<li><a href=\"\">Safe Shipping<\/a><\/li>\r\n<li><a href=\"\">Returns & Exchanges<\/a><\/li>\r\n<li><a href=\"\">Help & FAQs<\/a><\/li>\r\n<li><a href=\"\">Warranties & Guarantees<\/a><\/li>\r\n<li><a href=\"\/\">Extended Service Plan<\/a><\/li>\r\n<li><a href=\"\">Contact Us<\/a><\/li>\r\n<\/ul>"},"custom_html-3":{"title":"SERVICE OFFERS","content":"<ul class=\"links\">\r\n<li><a href=\"\">Repairs & Maintenance<\/a><\/li>\r\n<li><a href=\"\"> Custom Design <\/a><\/li>\r\n<li><a href=\"\">Help & FAQs<\/a><\/li>\r\n<li><a href=\"\">Studio Appraisals<\/a><\/li>\r\n<li><a href=\"\/\">Appraisals Change<\/a><\/li>\r\n<li><a href=\"\">Gold Exchange<\/a><\/li>\r\n<\/ul>"},"custom_html-4":{"title":"ABOUT US","content":"<ul class=\"links\">\r\n<li><a href=\"\">Our Company<\/a><\/li>\r\n<li><a href=\"\">Careers<\/a><\/li>\r\n<li><a href=\"\">Affiliate Program<\/a><\/li>\r\n<li><a href=\"\">Information<\/a><\/li>\r\n<li><a href=\"\/\">Coupons & Offers<\/a><\/li>\r\n<li><a href=\"\">In-Store Events<\/a><\/li>\r\n<\/ul>"},"custom_html-5":{"title":"","content":"<img  src=\"http:\/\/themepiko.com\/demo\/stock\/jewelry\/wp-content\/uploads\/2017\/10\/jeewelry-logo-w.png\" alt=\"\"\/>\r\n<p style=\"padding-right:0px\">\r\nThis is easy to update text from footer widget area. Add here information about your store.<\/p>\r\n<p>    25 Southern Avenue, USA.<br>\r\n<abbr title=\"Phone Number\"><strong>Phone:<\/strong><\/abbr> (123) 4567 890<br>\r\n<abbr title=\"Fax Numbers\"><strong>Fax:<\/strong><\/abbr> (123) 4567 890-12<br>\r\n<abbr title=\"Email Address\"><strong>Email:<\/strong><\/abbr> info@company.com<br>\r\n<\/p>\r\n"}},"sidebar-5":{"custom_html-6":{"title":"","content":"<img  src=\"http:\/\/themepiko.com\/demo\/stock\/jewelry\/wp-content\/uploads\/2017\/10\/jeewelry-logo-w.png\" alt=\"\"\/>\r\n<p style=\"padding-right:30px\">\r\nThis is easy to update text from footer widget area. Add here information about your store.<\/p>\r\n<p>    25 Southern Avenue, USA.<br>\r\n<abbr title=\"Phone Number\"><strong>Phone:<\/strong><\/abbr> (123) 4567 890<br>\r\n<abbr title=\"Fax Numbers\"><strong>Fax:<\/strong><\/abbr> (123) 4567 890-12<br>\r\n<abbr title=\"Email Address\"><strong>Email:<\/strong><\/abbr> info@company.com<br>\r\n<\/p>"},"custom_html-7":{"title":"CUSTOMER SERVICES","content":"<ul class=\"links\">\r\n<li><a href=\"\">Safe Shipping<\/a><\/li>\r\n<li><a href=\"\">Returns & Exchanges<\/a><\/li>\r\n<li><a href=\"\">Help & FAQs<\/a><\/li>\r\n<li><a href=\"\">Warranties & Guarantees<\/a><\/li>\r\n<li><a href=\"\/\">Extended Service Plan<\/a><\/li>\r\n<li><a href=\"\">Contact Us<\/a><\/li>\r\n<\/ul>"},"custom_html-8":{"title":"SERVICE OFFERS","content":"<ul class=\"links\">\r\n<li><a href=\"\">Repairs & Maintenance<\/a><\/li>\r\n<li><a href=\"\"> Custom Design <\/a><\/li>\r\n<li><a href=\"\">Help & FAQs<\/a><\/li>\r\n<li><a href=\"\">Studio Appraisals<\/a><\/li>\r\n<li><a href=\"\/\">Appraisals Change<\/a><\/li>\r\n<li><a href=\"\">Gold Exchange<\/a><\/li>\r\n<\/ul>"},"custom_html-9":{"title":"","content":"Subscribe to our newsletter and get 15% off your first three Product<p><\/p>\r\n\r\n[contact-form-7 id=\"4439\" title=\"Subscribe Form\"]\r\n<p>We are support getway<\/p>\r\n<img  src=\"http:\/\/themepiko.com\/demo\/stock\/default\/wp-content\/uploads\/2017\/04\/payment.png\" \/>"}},"sidebar-6":{"woocommerce_products-2":{"title":"NEW ARAIVALS","number":3,"show":"","orderby":"date","order":"desc","hide_free":0,"show_hidden":0},"woocommerce_products-3":{"title":"FEATURED PRODUCT","number":3,"show":"featured","orderby":"date","order":"desc","hide_free":0,"show_hidden":0},"woocommerce_top_rated_products-2":{"title":"TOP RATED PRODUCT","number":3},"woocommerce_products-4":{"title":"ON-SALE PRODUCT","number":3,"show":"onsale","orderby":"date","order":"desc","hide_free":0,"show_hidden":0}},"sidebar-4":{"woocommerce_product_categories-2":{"title":"Product categories","orderby":"name","dropdown":0,"count":0,"hierarchical":1,"show_children_only":0,"hide_empty":0,"max_depth":""},"woocommerce_top_rated_products-3":{"title":"Top rated products","number":3}},"sidebar-7":{"tag_cloud-2":{"title":"Jewelry Categoy","count":0,"taxonomy":"product_cat"},"tag_cloud-3":{"title":"Filter Jewelry Brand","count":0,"taxonomy":"brand"},"piko-socials-icon-2":{"title":"Our Social Pages"}},"sidebar-8":{"custom_html-10":{"title":"","content":"<div style=\"background-image: url(http:\/\/themepiko.com\/demo\/stock\/jewelry\/wp-content\/uploads\/2017\/10\/shop-bg2.jpg) !important;background-position: 0 0 !important;background-repeat: no-repeat !important\">\r\n[brand_logo images=\"4724,4723\"  onclick=\"custom_link\" is_slider=\"yes\" navigation=\"false\" dots=\"true\" autoplay=\"true\" loop=\"true\" use_responsive=\"0\" custom_links=\"http:\/\/themepiko.com\/demo\/stock\/jewelry\/product-category\/earrings,http:\/\/themepiko.com\/demo\/stock\/jewelry\/product-category\/engagement\"]\r\n<\/div>"}}}';
                       }elseif($version == 'dokan'){ 
                           $data = '{"sidebar-1":{"recent_postimage-1":{"title":"Latest Posts","show":"3"},"categories-3":{"title":"Categories","count":0,"hierarchical":0,"dropdown":0},"tag_cloud-1":{"title":false,"taxonomy":"post_tag"},"null-instagram-feed-1":{"title":"Instagram","username":"piko.stock","number":"6","columns":3,"size":"thumbnail","target":"_blank","link":"","slider":false,"spacing":false},"piko-socials-icon-1":{"title":"Socials"}},"sidebar-3":{"custom_html-1":{"title":"","content":"<img  src=\"http:\/\/themepiko.com\/demo\/stock\/default\/wp-content\/uploads\/2016\/12\/logo.png\" alt=\"\"\/>\r\n<p style=\"padding-right:30px\">\r\nThis is easy to update text from footer widget area. Add here information about your store.<\/p>\r\n<p>    25 Southern Avenue, USA.<br>\r\n<abbr title=\"Phone Number\"><strong>Phone:<\/strong><\/abbr> (123) 4567 890<br>\r\n<abbr title=\"Fax\"><strong>Fax:<\/strong><\/abbr> (123) 4567 890 22<br>\r\n<abbr title=\"Email Address\"><strong>Email:<\/strong><\/abbr> info@company.com<br>\r\n<\/p>\r\n"},"custom_html-2":{"title":"","content":"<div class=\"row\">\r\n\t<div class=\"col-md-6\">\r\n<ul class=\"links\">\r\n<li><a href=\"#\">FAQs<\/a><\/li>\r\n\t<li><a href=\"#\">Careers<\/a><\/li>\r\n<li><a href=\"#\">My Account<\/a><\/li>\r\n<li><a href=\"#\">Orders Tracking<\/a><\/li>\r\n<li><a href=\"#\">Store Location<\/a><\/li>\r\n<li><a href=\"#\">Legal Notice<\/a><\/li>\r\n<\/ul>\r\n\t<\/div>\r\n\t<div class=\"col-md-6\">\r\n\t\t<ul class=\"links\">\r\n<li><a href=\"#\">About Us<\/a><\/li>\r\n<li><a href=\"#\">Contact Us<\/a><\/li>\r\n<li><a href=\"#\">Terms & Conditions<\/a><\/li>\r\n<li><a href=\"#\">Privacy Policy<\/a><\/li>\r\n<li><a href=\"#\">Returns & Exchanges<\/a><\/li>\r\n<li><a href=\"#\">Shipping & Delivery<\/a><\/li>\r\n<\/ul>\r\n\t<\/div>\r\n<\/div>"},"custom_html-5":{"title":"","content":"Subscribe to our newsletter and get 15% off your first three Product<p><\/p>\r\n\r\n[contact-form-7 id=\"4439\" title=\"Subscribe Form\"]\r\n<p><\/p>\r\n"}},"sidebar-5":{"woocommerce_top_rated_products-1":{"title":"Top Rated","number":3},"woocommerce_products-1":{"title":"On Sale","number":3,"show":"onsale","orderby":"rand","order":"desc","hide_free":0,"show_hidden":0},"woocommerce_products-2":{"title":"Top Sellers","number":3,"show":"","orderby":"sales","order":"desc","hide_free":0,"show_hidden":0},"woocommerce_products-3":{"title":"Recomended","number":3,"show":"featured","orderby":"rand","order":"desc","hide_free":0,"show_hidden":0}},"sidebar-6":{"latest_tweets_widget-1":{"title":"Latest Tweets","screen_name":"seeyou38","num":"5","pop":"0"},"piko_flickr_widget-1":{"title":"Flickr Stream","screen_name":"48126477@N05","number":"6"},"null-instagram-feed-2":{"title":"Instagram","username":"piko.stock","number":"6","columns":"2","size":"thumbnail","target":"_blank","link":"Follow Us","slider":false,"spacing":true},"tag_cloud-2":{"title":"Product Categories","taxonomy":"product_cat"},"nav_menu-1":{"nav_menu":56}},"sidebar-4":{"pikoworks_widget_brands-1":{"title":"Filter by Brand","displayType":"image","dropdown":0,"count":1,"hide_empty":0},"woocommerce_price_filter-1":{"title":"Filter by price"},"woocommerce_product_categories-1":{"title":"Product categories","orderby":"name","dropdown":0,"count":0,"hierarchical":1,"show_children_only":0,"hide_empty":0},"woocommerce_top_rated_products-2":{"title":"Top Rated","number":3},"tag_cloud-3":{"title":false,"taxonomy":"product_tag"}},"sidebar-7":{"pikoworks_widget_brands-4":{"title":"Filter by brand","displayType":"name","dropdown":0,"count":1,"hide_empty":0},"yith-woo-ajax-navigation-list-price-filter-2":{"title":"Filter By Price","dropdown":1,"dropdown_type":"open","prices":[{"min":"0","max":"100"},{"min":"100","max":"150"},{"min":"150","max":"200"},{"min":"200","max":"300"},{"min":"300","max":"999"}]},"yith-woo-ajax-navigation-2":{"title":"Filter By Color","attribute":"color","query_type":"and","type":"color","colors":{"71":"#2d2d2d","72":"#26c9f2","73":"#3dc64f"},"multicolor":[],"labels":[],"display":"all","extra_class":"","style":"square","show_count":0,"dropdown":0,"dropdown_type":"open","tags_list":[],"tags_list_query":"exclude","see_all_tax_text":""},"yith-woo-ajax-navigation-3":{"title":"Filter By Size","attribute":"size","query_type":"and","type":"label","colors":[],"multicolor":[],"labels":{"76":"L","75":"M","74":"S","77":"XL"},"display":"all","extra_class":"","style":"square","show_count":0,"dropdown":0,"dropdown_type":"open","tags_list":[],"tags_list_query":"exclude","see_all_tax_text":""},"yith-woo-ajax-navigation-stock-on-sale-2":{"title":"Stock\/On sale","onsale":1,"instock":1,"dropdown":1,"dropdown_type":"open"},"yith-woo-ajax-reset-navigation-2":{"title":"","label":"Reset All Filters","custom_style":0,"background_color":"","background_color_hover":"","text_color":"","text_color_hover":"","border_color":"","border_color_hover":""},"piko-socials-icon-2":{"title":"Socials"}},"sidebar-8":{"woocommerce_product_categories-2":{"title":"","orderby":"name","dropdown":0,"count":0,"hierarchical":0,"show_children_only":1,"hide_empty":0}}}';
                       }elseif($version == 'vendor'){ 
                           $data = '{"sidebar-2":{"bp_core_members_widget-2":{"title":"Members","max_members":"5","member_default":"active","link_title":false},"bp_groups_widget-2":{"title":"Groups","max_groups":"5","group_default":"active","link_title":false},"bp_core_friends_widget-2":{"max_friends":5,"friend_default":"active","link_title":false},"bp_core_whos_online_widget-2":{"title":"Who s Online","max_members":"15"}},"sidebar-3":{"custom_html-6":{"title":"","content":"<h2 class=\"widget-title mb5\">Online Shopping by New York UK<\/h2>\r\nXtocky is a leading e-commerce player in the country, is creating a colossal shift in xtokcy buying habits. It continues to evolve and adapt to the needs of its customers by continuously streamlining website and mobile app design, constantly growing the inventory of items, and offering convenient payment options. All these result in a best-in-class online store that delivers effortless shopping experience to every xtocky."},"custom_html-4":{"title":"INFORMATION","content":"<div class=\"row\">\r\n\t<div class=\"col-md-6\">\r\n<ul class=\"links\">\r\n<li><a href=\"#\">My Account<\/a><\/li>\r\n<li><a href=\"#\">Support Center<\/a><\/li>\r\n<li><a href=\"#\">Exchanges<\/a><\/li>\r\n<li><a href=\"#\">Shipping<\/a><\/li>\r\n<li><a href=\"#\">Privacy Policy<\/a><\/li>\r\n<li><a href=\"#\">Terms & Conditions<\/a><\/li>\r\n<\/ul>\r\n\t<\/div>\r\n<div class=\"col-md-6\">\r\n\t<ul class=\"links\">\r\n<li><a href=\"#\">Shop Location<\/a><\/li>\r\n<li><a href=\"#\">Account Info<\/a><\/li>\r\n<li><a href=\"#\">Product Tracking<\/a><\/li>\r\n<li><a href=\"#\">Job Vacancy <\/a><\/li>\r\n<li><a href=\"#\">Question<\/a><\/li>\r\n<li><a href=\"#\">Help<\/a><\/li>\r\n<\/ul>\r\n\t<\/div>\r\n<\/div>"},"custom_html-2":{"title":"TOP CATEGORIES","content":"<div class=\"row\">\r\n\t<div class=\"col-md-6\">\r\n\t<ul class=\"links\">\r\n<li><a href=\"#\">Laptop<\/a><\/li>\r\n<li><a href=\"#\">Phones & Tablets<\/a><\/li>\r\n<li><a href=\"#\">TV & Audio<\/a><\/li>\r\n<li><a href=\"#\">Gadgets<\/a><\/li>\r\n<li><a href=\"#\">Software<\/a><\/li>\r\n<li><a href=\"#\">CC Camera<\/a><\/li>\t\t\r\n<\/ul>\r\n\t<\/div>\r\n<div class=\"col-md-6\">\r\n\t<ul class=\"links\">\r\n<li><a href=\"#\">Computers & Laptops<\/a><\/li>\r\n<li><a href=\"#\">Photo & Cameras<\/a><\/li>\r\n<li><a href=\"#\">Mobile & Accessories<\/a><\/li>\r\n<li><a href=\"#\">Car Electronic<\/a><\/li>\r\n\t\t<li><a href=\"#\">Printers & Ink<\/a><\/li>\r\n\t\t<li><a href=\"#\">Games<\/a><\/li>\r\n<\/ul>\r\n\t<\/div>\r\n<\/div>\r\n"}},"sidebar-5":{"woocommerce_products-3":{"title":"RECOMENDED PRODUCT","number":3,"show":"","orderby":"rand","order":"desc","hide_free":0,"show_hidden":0},"woocommerce_products-4":{"title":"ON SALES PRODUCT","number":3,"show":"onsale","orderby":"date","order":"desc","hide_free":0,"show_hidden":0},"woocommerce_top_rated_products-2":{"title":"TOP RATED PRODUCT","number":3},"woocommerce_products-2":{"title":"TOP SELLERS PRODUCT","number":3,"show":"","orderby":"sales","order":"desc","hide_free":0,"show_hidden":0}},"sidebar-6":{"custom_html-5":{"title":"","content":"[vc_row full_width=\"stretch_row\"][vc_column width=\"7\/12\"][vc_row_inner][vc_column_inner width=\"1\/12\"][\/vc_column_inner][vc_column_inner width=\"11\/12\"][vc_custom_heading title_font_size=\"fs25\" text=\"Stay with us!\" font_container=\"tag:div|text_align:left|color:%23ffffff|line_height:1.2\" use_theme_fonts=\"yes\" sub_title=\"Subscribe & get 25% off your first three order.\"][\/vc_column_inner][\/vc_row_inner][\/vc_column][vc_column width=\"5\/12\"][vc_column_text][contact-form-7 id=\"4439\" title=\"Subscribe Form\"][\/vc_column_text][\/vc_column][\/vc_row]"}},"sidebar-4":{"woocommerce_product_categories-3":{"title":"Product categories","orderby":"name","dropdown":0,"count":1,"hierarchical":1,"show_children_only":1,"hide_empty":1,"max_depth":""},"pikoworks_widget_brands-3":{"title":"Filter by brand","displayType":"image","dropdown":0,"count":1,"hide_empty":0},"woocommerce_products-5":{"title":"Offer Products","number":3,"show":"onsale","orderby":"date","order":"desc","hide_free":0,"show_hidden":0},"woocommerce_recently_viewed_products-2":{"title":"Recently Viewed Products","number":3}},"sidebar-7":{"pikoworks_widget_brands-2":{"title":"Filter by brand","displayType":"name","dropdown":0,"count":1,"hide_empty":1},"yith-woo-ajax-navigation-list-price-filter-2":{"title":"Price Filter","dropdown":0,"dropdown_type":"open","prices":[{"min":"0","max":"100"},{"min":"50","max":"150"},{"min":"150","max":"250"},{"min":"250","max":"350"},{"min":"400","max":"1000"}]},"yith-woo-ajax-navigation-stock-on-sale-2":{"title":"Stock\/On sale","onsale":1,"instock":1,"dropdown":0,"dropdown_type":"open"},"yith-woo-ajax-reset-navigation-2":{"title":"","label":"Reset All Filters","custom_style":0,"background_color":"","background_color_hover":"","text_color":"","text_color_hover":"","border_color":"","border_color_hover":""},"piko-socials-icon-3":{"title":"Our Socials Pages"}},"sidebar-8":{"custom_html-7":{"title":"","content":"[rev_slider alias=\"Vendor-catalog\"]"}}}';
                       }elseif($version == 'rtl'){
                           $data = '{"sidebar-1":{"recent_postimage-2":{"title":"\u0622\u062e\u0631 \u0627\u0644\u0645\u0634\u0627\u0631\u0643\u0627\u062a","show":"3"},"categories-2":{"title":"","count":0,"hierarchical":0,"dropdown":0},"recent-posts-2":{"title":"","number":5},"archives-2":{"title":"","count":0,"dropdown":0},"piko-socials-icon-2":{"title":"\u0635\u0641\u062d\u0627\u062a \u0633\u0648\u0633\u064a\u0627\u0644\u0632"}},"sidebar-3":{"custom_html-1":{"title":"","content":"<h2 class=\"widget-title mb5\" style=\"margin-left:40px\">\u0627\u0644\u062a\u0633\u0648\u0642 \u0639\u0628\u0631 \u0627\u0644\u0625\u0646\u062a\u0631\u0646\u062a \u0645\u0646 \u0642\u0628\u0644 \u0627\u0644\u0645\u0645\u0644\u0643\u0629 \u0627\u0644\u0639\u0631\u0628\u064a\u0629 \u0627\u0644\u0633\u0639\u0648\u062f\u064a\u0629 \u0627\u0644\u062c\u062f\u064a\u062f\u0629<\/h2>\r\n<p style=\"margin-left:30px\">\r\n\u0632\u062a\u0648\u0643\u064a \u0647\u064a \u0627\u0644\u0631\u0627\u0626\u062f\u0629 \u0641\u064a \u0645\u062c\u0627\u0644 \u0627\u0644\u062a\u062c\u0627\u0631\u0629 \u0627\u0644\u0625\u0644\u0643\u062a\u0631\u0648\u0646\u064a\u0629 \u0644\u0627\u0639\u0628 \u0641\u064a \u0627\u0644\u0628\u0644\u0627\u062f\u060c \u0648\u062e\u0644\u0642 \u062a\u062d\u0648\u0644 \u0647\u0627\u0626\u0644 \u0641\u064a \u0639\u0627\u062f\u0627\u062a \u0634\u0631\u0627\u0621 \u0634\u062a\u0648\u0643\u064a. \u0627\u0646\u0647\u0627 \u0644\u0627 \u062a\u0632\u0627\u0644 \u062a\u062a\u0637\u0648\u0631 \u0648\u0627\u0644\u062a\u0643\u064a\u0641 \u0645\u0639 \u0627\u062d\u062a\u064a\u0627\u062c\u0627\u062a \u0639\u0645\u0644\u0627\u0626\u0647\u0627 \u0645\u0646 \u062e\u0644\u0627\u0644 \u062a\u0628\u0633\u064a\u0637 \u0628\u0627\u0633\u062a\u0645\u0631\u0627\u0631 \u0627\u0644\u0645\u0648\u0642\u0639 \u0648\u062a\u0635\u0645\u064a\u0645 \u062a\u0637\u0628\u064a\u0642\u0627\u062a \u0627\u0644\u062c\u0648\u0627\u0644\u060c \u0648\u062a\u0646\u0645\u0648 \u0628\u0627\u0633\u062a\u0645\u0631\u0627\u0631 \u0627\u0644\u0645\u062e\u0632\u0648\u0646 \u0645\u0646 \u0627\u0644\u0628\u0646\u0648\u062f\u060c \u0648\u062a\u0642\u062f\u064a\u0645 \u062e\u064a\u0627\u0631\u0627\u062a \u0627\u0644\u062f\u0641\u0639 \u0645\u0631\u064a\u062d\u0629. \u0643\u0644 \u0647\u0630\u0647 \u0627\u0644\u0646\u062a\u0627\u0626\u062c \u0641\u064a \u0623\u0641\u0636\u0644 \u0641\u064a \u0641\u0626\u062a\u0647\u0627 \u0645\u062a\u062c\u0631 \u0639\u0644\u0649 \u0627\u0644\u0627\u0646\u062a\u0631\u0646\u062a \u0627\u0644\u062a\u064a \u062a\u0648\u0641\u0631 \u062a\u062c\u0631\u0628\u0629 \u062a\u0633\u0648\u0642 \u062c\u0647\u062f \u0644\u0643\u0644 \u0634\u062a\u0648\u0643\u064a.<\/p>"},"custom_html-2":{"title":"\u0645\u0639\u0644\u0648\u0645\u0627\u062a","content":"<div class=\"row\">\r\n\t<div class=\"col-md-6\">\r\n<ul class=\"links\">\r\n<li><a href=\"#\">\u062d\u0633\u0627\u0628\u064a<\/a><\/li>\r\n<li><a href=\"#\">\u0645\u0631\u0643\u0632 \u0627\u0644\u062f\u0639\u0645<\/a><\/li>\r\n<li><a href=\"#\">\u0627\u0644\u062a\u0628\u0627\u062f\u0644<\/a><\/li>\r\n<li><a href=\"#\">\u0627\u0644\u0634\u062d\u0646<\/a><\/li>\r\n<li><a href=\"#\">\u0633\u064a\u0627\u0633\u0629 \u0627\u0644\u062e\u0635\u0648\u0635\u064a\u0629<\/a><\/li>\r\n<li><a href=\"#\">\u0627\u0644\u0628\u0646\u0648\u062f \u0648 \u0627\u0644\u0638\u0631\u0648\u0641<\/a><\/li>\r\n<\/ul>\r\n\t<\/div>\r\n<div class=\"col-md-6\">\r\n\t<ul class=\"links\">\r\n<li><a href=\"#\">\u0645\u0648\u0642\u0639 \u0627\u0644\u0645\u062a\u062c\u0631<\/a><\/li>\r\n<li><a href=\"#\">\u0645\u0639\u0644\u0648\u0645\u0627\u062a \u0627\u0644\u062d\u0633\u0627\u0628<\/a><\/li>\r\n<li><a href=\"#\">\u062a\u062a\u0628\u0639 \u0627\u0644\u0645\u0646\u062a\u062c<\/a><\/li>\r\n<li><a href=\"#\">\u0648\u0638\u064a\u0641\u0629 \u0634\u0627\u063a\u0631\u0629<\/a><\/li>\r\n<li><a href=\"#\">\u0633\u0624\u0627\u0644<\/a><\/li>\r\n<li><a href=\"#\">\u0645\u0633\u0627\u0639\u062f\u0629<\/a><\/li>\r\n<\/ul>\r\n\t<\/div>\r\n<\/div>"},"custom_html-3":{"title":"\u0627\u0644\u0641\u0626\u0627\u062a \u0627\u0644\u0623\u0639\u0644\u0649","content":"<div class=\"row\">\r\n\t<div class=\"col-md-6\">\r\n\t<ul class=\"links\">\r\n<li><a href=\"#\">Laptop<\/a><\/li>\r\n<li><a href=\"#\">Phones & Tablets<\/a><\/li>\r\n<li><a href=\"#\">TV & Audio<\/a><\/li>\r\n<li><a href=\"#\">Gadgets<\/a><\/li>\r\n<li><a href=\"#\">Software<\/a><\/li>\r\n<li><a href=\"\u0645\u0633\u0627\u0639\u062f\u0629\">CC Camera<\/a><\/li>\t\t\r\n<\/ul>\r\n\t<\/div>\r\n<div class=\"col-md-6\">\r\n\t<ul class=\"links\">\r\n<li><a href=\"#\">Computers & Laptops<\/a><\/li>\r\n<li><a href=\"#\">Photo & Cameras<\/a><\/li>\r\n<li><a href=\"#\">Mobile & Accessories<\/a><\/li>\r\n<li><a href=\"#\">Car Electronic<\/a><\/li>\r\n\t\t<li><a href=\"#\">Printers & Ink<\/a><\/li>\r\n\t\t<li><a href=\"#\/\">Games<\/a><\/li>\r\n<\/ul>\r\n\t<\/div>\r\n<\/div>\r\n"}},"sidebar-5":{"woocommerce_products-1":{"title":"\u0627\u0644\u0645\u0646\u062a\u062c \u0627\u0644\u0645\u0648\u0635\u0649 \u0628\u0647","number":3,"show":"","orderby":"rand","order":"desc","hide_free":0,"show_hidden":0},"woocommerce_products-2":{"title":"\u0639\u0644\u0649 \u0627\u0644\u0645\u0628\u064a\u0639\u0627\u062a \u0627\u0644\u0645\u0646\u062a\u062c","number":3,"show":"onsale","orderby":"date","order":"desc","hide_free":0,"show_hidden":0},"woocommerce_top_rated_products-1":{"title":"\u062a\u0648\u0628 \u0631\u0627\u062a\u062f \u0627\u0644\u0645\u0646\u062a\u062c","number":3},"woocommerce_products-3":{"title":"\u0623\u0639\u0644\u0649 \u0627\u0644\u0628\u0627\u0626\u0639\u064a\u0646 \u0627\u0644\u0645\u0646\u062a\u062c","number":3,"show":"","orderby":"sales","order":"desc","hide_free":0,"show_hidden":0}},"sidebar-6":{"custom_html-4":{"title":"","content":"[vc_row][vc_column width=\"1\/12\"][\/vc_column][vc_column width=\"5\/6\"][vc_row_inner][vc_column_inner width=\"1\/2\"][vc_column_text][contact-form-7 id=\"4439\" title=\"Subscribe Form\"][\/vc_column_text][\/vc_column_inner][vc_column_inner width=\"1\/2\"][vc_custom_heading title_font_size=\"fs25\" text=\"\u0627\u0628\u0642\u0649 \u0645\u0639\u0646\u0627!\" font_container=\"tag:div|text_align:right|color:%23222222|line_height:1.2\" use_theme_fonts=\"yes\" sub_title=\"\u0627\u0634\u062a\u0631\u0643 \u0648\u0627\u062d\u0635\u0644 \u0639\u0644\u0649 \u062e\u0635\u0645 25\u066a \u0639\u0644\u0649 \u0623\u0648\u0644 \u0637\u0644\u0628 \u0644\u0643.\"][\/vc_column_inner][\/vc_row_inner][\/vc_column][vc_column width=\"1\/12\"][\/vc_column][\/vc_row]"}},"sidebar-4":{"yith-woo-ajax-navigation-list-price-filter-1":{"title":"\u062a\u0635\u0641\u064a\u0629 \u0627\u0644\u0623\u0633\u0639\u0627\u0631","dropdown":0,"dropdown_type":"open","prices":[{"min":"0","max":"100"},{"min":"50","max":"150"},{"min":"150","max":"250"},{"min":"250","max":"350"},{"min":"400","max":"1000"}]},"yith-woo-ajax-navigation-stock-on-sale-1":{"title":"\u0627\u0644\u0623\u0633\u0647\u0645 \/ \u0644\u0644\u0628\u064a\u0639","onsale":1,"instock":1,"dropdown":0,"dropdown_type":"open"},"yith-woo-ajax-reset-navigation-1":{"title":"","label":"\u0625\u0639\u0627\u062f\u0629 \u062a\u0639\u064a\u064a\u0646 \u062c\u0645\u064a\u0639 \u0627\u0644\u0641\u0644\u0627\u062a\u0631","custom_style":0,"background_color":"","background_color_hover":"","text_color":"","text_color_hover":"","border_color":"","border_color_hover":""},"woocommerce_product_categories-1":{"title":"\u0641\u0626\u0627\u062a \u0627\u0644\u0645\u0646\u062a\u062c\u0627\u062a","orderby":"name","dropdown":0,"count":1,"hierarchical":1,"show_children_only":1,"hide_empty":1,"max_depth":""},"woocommerce_products-4":{"title":"\u0639\u0631\u0636 \u0627\u0644\u0645\u0646\u062a\u062c\u0627\u062a","number":3,"show":"onsale","orderby":"date","order":"desc","hide_free":0,"show_hidden":0},"piko-socials-icon-1":{"title":"\u0644\u062f\u064a\u0646\u0627 \u0635\u0641\u062d\u0627\u062a \u0627\u0644\u0627\u062c\u062a\u0645\u0627\u0639\u064a\u0629"},"woocommerce_recently_viewed_products-1":{"title":"\u0627\u0644\u0645\u0646\u062a\u062c\u0627\u062a \u0627\u0644\u0645\u0639\u0631\u0648\u0636\u0629 \u0645\u0624\u062e\u0631\u0627","number":3}},"sidebar-8":{"custom_html-5":{"title":"","content":"[rev_slider alias=\"Vendor-catalog\"]"}}}';
                       }
                           
                        $data = json_decode($data);                            
                            
                        global $wp_registered_sidebars, $wp_registered_widget_controls;

                        // Have valid data?
                        // If no data or could not decode
                        if ( empty( $data ) || ! is_object( $data ) ) {
                                wp_die(		
                                        esc_html__( 'json file Empty', 'pikoworks_core' ),
                                        '',
                                        array( 'back_link' => true )
                                );
                        }
                        echo '<p>' .esc_html__( 'Widgets Successfully imported', 'pikoworks_core' ) . '</p>';

                        $widget_controls = $wp_registered_widget_controls;

                        $available_widgets = array();

                        foreach ( $widget_controls as $widget ) {

                                if ( ! empty( $widget['id_base'] ) && ! isset( $available_widgets[$widget['id_base']] ) ) { // no dupes

                                        $available_widgets[$widget['id_base']]['id_base'] = $widget['id_base'];
                                        $available_widgets[$widget['id_base']]['name'] = $widget['name'];

                                }

                        }


                        // Get all available widgets site supports	

                        // Get all existing widget instances
                        $widget_instances = array();
                        foreach ( $available_widgets as $widget_data ) {
                                $widget_instances[$widget_data['id_base']] = get_option( 'widget_' . $widget_data['id_base'] );
                        }

                        // Begin results
                        $results = array();

                        // Loop import data's sidebars
                        foreach ( $data as $sidebar_id => $widgets ) {

                                // Skip inactive widgets
                                // (should not be in export file)
                                if ( 'wp_inactive_widgets' == $sidebar_id ) {
                                        continue;
                                }

                                // Check if sidebar is available on this site
                                // Otherwise add widgets to inactive, and say so
                                if ( isset( $wp_registered_sidebars[$sidebar_id] ) ) {
                                        $sidebar_available = true;
                                        $use_sidebar_id = $sidebar_id;
                                        $sidebar_message_type = 'success';
                                        $sidebar_message = '';
                                } else {
                                        $sidebar_available = false;
                                        $use_sidebar_id = 'wp_inactive_widgets'; // add to inactive if sidebar does not exist in theme
                                        $sidebar_message_type = 'error';
                                        $sidebar_message = esc_html__( 'Widget area does not exist in theme (using Inactive)', 'pikoworks_core' );
                                }

                                // Result for sidebar
                                $results[$sidebar_id]['name'] = ! empty( $wp_registered_sidebars[$sidebar_id]['name'] ) ? $wp_registered_sidebars[$sidebar_id]['name'] : $sidebar_id; // sidebar name if theme supports it; otherwise ID
                                $results[$sidebar_id]['message_type'] = $sidebar_message_type;
                                $results[$sidebar_id]['message'] = $sidebar_message;
                                $results[$sidebar_id]['widgets'] = array();

                                // Loop widgets
                                foreach ( $widgets as $widget_instance_id => $widget ) {

                                        $fail = false;

                                        // Get id_base (remove -# from end) and instance ID number
                                        $id_base = preg_replace( '/-[0-9]+$/', '', $widget_instance_id );
                                        $instance_id_number = str_replace( $id_base . '-', '', $widget_instance_id );

                                        // Does site support this widget?
                                        if ( ! $fail && ! isset( $available_widgets[$id_base] ) ) {
                                                $fail = true;
                                                $widget_message_type = 'error';
                                                $widget_message = esc_html__( 'Site does not support widget', 'pikoworks_core' ); // explain why widget not imported
                                        }

                                        // Filter to modify settings object before conversion to array and import
                                        $widget = apply_filters( 'wie_widget_settings', $widget ); // object

                                        // Convert multidimensional objects to multidimensional arrays			
                                        $widget = json_decode( wp_json_encode( $widget ), true );


                                        // Does widget with identical settings already exist in same sidebar?
                                        if ( ! $fail && isset( $widget_instances[$id_base] ) ) {

                                                // Get existing widgets in this sidebar
                                                $sidebars_widgets = get_option( 'sidebars_widgets' );
                                                $sidebar_widgets = isset( $sidebars_widgets[$use_sidebar_id] ) ? $sidebars_widgets[$use_sidebar_id] : array(); // check Inactive if that's where will go

                                                // Loop widgets with ID base
                                                $single_widget_instances = ! empty( $widget_instances[$id_base] ) ? $widget_instances[$id_base] : array();
                                                foreach ( $single_widget_instances as $check_id => $check_widget ) {

                                                        // Is widget in same sidebar and has identical settings?
                                                        if ( in_array( "$id_base-$check_id", $sidebar_widgets ) && (array) $widget == $check_widget ) {

                                                                $fail = true;
                                                                $widget_message_type = 'warning';
                                                                $widget_message = esc_html__( 'Widget already exists', 'pikoworks_core' ); // explain why widget not imported

                                                                break;

                                                        }

                                                }

                                        }

                                        // No failure
                                        if ( ! $fail ) {

                                                // Add widget instance
                                                $single_widget_instances = get_option( 'widget_' . $id_base ); // all instances for that widget ID base, get fresh every time
                                                $single_widget_instances = ! empty( $single_widget_instances ) ? $single_widget_instances : array( '_multiwidget' => 1 ); // start fresh if have to
                                                $single_widget_instances[] = $widget; // add it

                                                        // Get the key it was given
                                                        end( $single_widget_instances );
                                                        $new_instance_id_number = key( $single_widget_instances );

                                                        // If key is 0, make it 1
                                                        // When 0, an issue can occur where adding a widget causes data from other widget to load, and the widget doesn't stick (reload wipes it)
                                                        if ( '0' === strval( $new_instance_id_number ) ) {
                                                                $new_instance_id_number = 1;
                                                                $single_widget_instances[$new_instance_id_number] = $single_widget_instances[0];
                                                                unset( $single_widget_instances[0] );
                                                        }

                                                        // Move _multiwidget to end of array for uniformity
                                                        if ( isset( $single_widget_instances['_multiwidget'] ) ) {
                                                                $multiwidget = $single_widget_instances['_multiwidget'];
                                                                unset( $single_widget_instances['_multiwidget'] );
                                                                $single_widget_instances['_multiwidget'] = $multiwidget;
                                                        }

                                                        // Update option with new widget
                                                        update_option( 'widget_' . $id_base, $single_widget_instances );

                                                // Assign widget instance to sidebar
                                                $sidebars_widgets = get_option( 'sidebars_widgets' ); // which sidebars have which widgets, get fresh every time

                                                // Avoid rarely fatal error when the option is an empty string
                                                // https://github.com/churchthemes/pikoworks_core/pull/11
                                                if ( ! $sidebars_widgets ) {
                                                        $sidebars_widgets = array();
                                                }

                                                $new_instance_id = $id_base . '-' . $new_instance_id_number; // use ID number from new widget instance
                                                $sidebars_widgets[$use_sidebar_id][] = $new_instance_id; // add new instance to sidebar
                                                update_option( 'sidebars_widgets', $sidebars_widgets ); // save the amended data

                                                // After widget import action
                                                $after_widget_import = array(
                                                        'sidebar'           => $use_sidebar_id,
                                                        'sidebar_old'       => $sidebar_id,
                                                        'widget'            => $widget,
                                                        'widget_type'       => $id_base,
                                                        'widget_id'         => $new_instance_id,
                                                        'widget_id_old'     => $widget_instance_id,
                                                        'widget_id_num'     => $new_instance_id_number,
                                                        'widget_id_num_old' => $instance_id_number
                                                );				

                                                // Success message
                                                if ( $sidebar_available ) {
                                                        $widget_message_type = 'success';
                                                        $widget_message = esc_html__( 'Imported', 'pikoworks_core' );
                                                } else {
                                                        $widget_message_type = 'warning';
                                                        $widget_message = esc_html__( 'Imported to Inactive', 'pikoworks_core' );
                                                }

                                        }

                                        // Result for widget instance
                                        $results[$sidebar_id]['widgets'][$widget_instance_id]['name'] = isset( $available_widgets[$id_base]['name'] ) ? $available_widgets[$id_base]['name'] : $id_base; // widget name or ID if name not available (not supported by site)
                                        $results[$sidebar_id]['widgets'][$widget_instance_id]['title'] = ! empty( $widget['title'] ) ? $widget['title'] : esc_html__( 'No Title', 'pikoworks_core' ); // show "No Title" if widget instance is untitled
                                        $results[$sidebar_id]['widgets'][$widget_instance_id]['message_type'] = $widget_message_type;
                                        $results[$sidebar_id]['widgets'][$widget_instance_id]['message'] = $widget_message;

                                }

                        }

                }

		public function update_home_page() {
			$blog_id = get_page_by_title('Blog');
			$home_page = get_page_by_title('Home ' . $this->_version);
			$pageid = $home_page->ID;
			update_option( 'show_on_front', 'page' );
			update_option( 'page_on_front', $pageid );
			update_option( 'page_for_posts', $blog_id->ID );
		}
		public function update_options() {
			global $xtocky;

			if(!class_exists('ReduxFrameworkInstances')) return;

			$options_file = $this->_remote_folder . 'options.json';

			$new_options = wp_remote_get($options_file);

			if( ! is_wp_error( $new_options )) {

				$new_options = json_decode($new_options['body'], true);

				$new_options = wp_parse_args( $new_options, $xtocky );

				$redux = ReduxFrameworkInstances::get_instance( 'xtocky' );

				if ( isset ( $redux->validation_ran ) ) {
					unset ( $redux->validation_ran );
				}

				$redux->set_options( $redux->_validate_options( $new_options ) );
			}
		}
	}

	new pikoworks_import_data();
}