<?php  if ( ! defined('ABSPATH')) exit('No direct script access allowed');

return array(
    'main-width-content' => 'container',   
    'enable_minifile' => '1',  
    'menu_style' => '1'  
);
