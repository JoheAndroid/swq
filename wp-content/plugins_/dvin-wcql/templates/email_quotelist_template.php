<?php

$colspan=2;
$grand_total_qty = $grand_total_price = 0;
$quote_list ='<table cellspacing="1" cellpadding="1"  style="'.$dvin_wcql_email_tbl_style.'"><tr style="'. $dvin_wcql_email_tbl_hdr_style.'"><td>&nbsp;</td><td>'.__('PRODUCT','dvinwcql').'</td>';
$row_str = '<tr style="'.$dvin_wcql_email_tbl_row_style.'"><td align="left" style="'.$dvin_wcql_email_tbl_cell_style.'"><a href="%s">%s</a></td><td align="left" style="'.$dvin_wcql_email_tbl_cell_style.'">%s</td>';


//add SKU if applicable
if($add_sku_toemail == 'on') {
    $colspan++;
    $quote_list .='<td>'.__('SKU','dvinwcql').'</td>';
    $row_str .='<td align="left" style="'.$dvin_wcql_email_tbl_cell_style.'">%s</td>';	
}
//hide price column if set
if($add_price_col == 'on' ) {
    $colspan++;
    $quote_list .='<td>'.__('PRICE','dvinwcql').'</td>';
    $row_str .='<td align="left" style="'.$dvin_wcql_email_tbl_cell_style.'">%s</td>';
}
//hide qty column if set
if($no_qty != 'on') {
    $colspan++;
    $quote_list .='<td>'.__('QUANTITY','dvinwcql').'</td>';
    $row_str .='<td align="left" style="'.$dvin_wcql_email_tbl_cell_style.'">%s</td>';
}
//hide price column if set
if($add_price_col == 'on') {
    $colspan++;
    $quote_list .='<td>'.__('TOTAL','dvinwcql').'</td>';
    $row_str .='<td align="left" style="'.$dvin_wcql_email_tbl_cell_style.'">%s</td>';
}

$quote_list .='</tr>';
$row_str .='</tr>';

$qlist = array();
if(isset($dvin_qlist_products)) {
    $qlist_count[0]['cnt'] = count($dvin_qlist_products);
    $qlist = $dvin_qlist_products;
}

foreach($qlist as $values) {

    //if valid product then only continue
    if(!isset($values['product_id']))
        continue;

    //initialize to avoid notices
    if(!isset($values['variation_id']))
        $values['variation_id']=0; 

    if (isset($values['product_id']) && is_numeric($values['product_id'])) {

        $values['prod_id'] = $values['ID'] = $values['product_id'];
        $values['ID'] =  $values['product_id'];

    }else{
            $values['prod_id'] = $values['ID'] = $values['product_id'];
            if(!empty($values['variation_id']))
                $values['ID'] = $values['variation_id'];						
    }

    $product_obj = wc_get_product( $values['variation_id']!=0 ? $values['variation_id'] : $values['product_id']);
    if ($product_obj->exists()) {
        $image_str = $product_obj->get_image();
        $href_str = esc_url( get_permalink(apply_filters('woocommerce_in_cart_product', $values['prod_id'])) );
    }

    $product_name_str = apply_filters('woocommerce_in_cartproduct_obj_title', $product_obj->get_title(), $product_obj);
    if(!empty($values['variation_id'])){
        $product_name_str .= woocommerce_get_formatted_variation(unserialize($values['variation_data']),false);
    }

     //get price before showing the addon data as proce could change
        if(!isset($values['price']))
            $unit_price = get_quotelist_product_price( $product_obj );
        else
            $unit_price = $values['price'];

           //get price before showing the addon data as proce could change
         //if addons extensions active
            if(class_exists('Product_Addon_Cart')) {
                $addon_cart_obj = new Product_Addon_Cart();
                $temp_arr = dvin_wcql_get_add_on_list($addon_cart_obj,$unit_price, $product_obj, $values);
            } else {
                $temp_arr = dvin_wcql_get_add_on_list(array(),$unit_price, $product_obj, $values);
            }

            $addons_str = wc_get_formatted_variation($temp_arr,false);  
      
    
    //if only set the price as visible NOT remove
    if($add_price_col == 'on' ) {

            $unit_price_in_html = apply_filters( 'woocommerce_cart_product_price', wc_price( $unit_price ), $product_obj );
            $unit_price_in_html = apply_filters( 'woocommerce_cart_item_price',$unit_price_in_html,'','');
    
        
         //decide the quantity
        $quantity = isset($_POST['qty'][$product_obj->id])?$_POST['qty'][$product_obj->id]:$values['quantity'];	

        $total_price = (float) $unit_price * (int)$quantity;
        $total_price_str = apply_filters('woocommerce_cart_item_price_html', woocommerce_price( $total_price)); 
    } else {
        //decide the quantity
        $quantity = isset($_POST['qty'][$product_obj->id])?$_POST['qty'][$product_obj->id]:$values['quantity'];	
    }



    //handle hiiden elements/columns
    $arr = array($href_str,$image_str,$product_name_str.$addons_str);
    if($add_sku_toemail == 'on') {
        $arr[] = $product_obj->get_sku();
    }
    if($add_price_col == 'on')	{		
            $arr[] = $unit_price_in_html;
    }
    if($dvin_wcql_settings['no_qty'] != 'on') {
        $arr[] = $quantity;
        $grand_total_qty +=$quantity;
    }
    if($add_price_col == 'on')	{		
        $arr[] = $total_price_str;				
        $grand_total_price += $total_price;
    }
    //echo 'kola'.sprintf($row_str,$arr);
    $quote_list .= vsprintf($row_str,$arr);

}

 if($add_price_col == 'on') { 
$row_str = '<tr style="'.$dvin_wcql_email_tbl_row_style.'"><td colspan="%s" align="right" style="'.$dvin_wcql_email_tbl_cell_style.'"><div  style="float:right;">%s</div></td>	<td align="left">&nbsp;%s</td><td align="right" style="'.$dvin_wcql_email_tbl_cell_style.'">%s</td></tr>';
$quote_list .= vsprintf($row_str,array($colspan-2,apply_filters('grand_total_text',__('GRAND TOTAL','dvinwcql')),$grand_total_qty,woocommerce_price($grand_total_price)));
}//end of grand total display

$quote_list .='</table>';
?>