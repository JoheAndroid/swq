<?php

/**
 * Dvin_Wcql_Shortcodes class, Handles the Listing shortcodes
 */
class Dvin_Wcql_Shortcodes {
	
	/**
     * function listing, retrieves quotelist products
	 * @param $atts Array - an associative array of attributes (preferences)
	 * @param $content  String -  the enclosed content
	 * @param $code String -  the shortcode name
	 * @code
	 * @static
     */
	static function listing($atts, $content = null, $code = "") {
		global $dvin_wcql_settings,$dvin_qlist_products;
		ob_start();
		echo '<div class="woocommerce">';
		$qlist = array();
		if((isset($dvin_qlist_products) && count($dvin_qlist_products)>0)) {
			$qlist_count[0]['cnt'] = count($dvin_qlist_products);
			$qlist = $dvin_qlist_products;
			//include the template file
			if(file_exists(TEMPLATEPATH . '/'. Dvin_Wcql::template_path().'templates/quotelist.php')) {
				include(TEMPLATEPATH . '/'. Dvin_Wcql::template_path().'templates/quotelist.php');
			}elseif(file_exists(STYLESHEETPATH . '/'. Dvin_Wcql::template_path().'templates/quotelist.php')) {
				include(STYLESHEETPATH . '/'. Dvin_Wcql::template_path().'templates/quotelist.php');
			}else{
				require('templates/quotelist.php');
			}
		} else {
	
			//include the template file
			if(file_exists(TEMPLATEPATH . '/'. Dvin_Wcql::template_path().'templates/list-empty.php')) {
				include(TEMPLATEPATH . '/'. Dvin_Wcql::template_path().'templates/list-empty.php');
			}elseif(file_exists(STYLESHEETPATH . '/'. Dvin_Wcql::template_path().'templates/list-empty.php')) {
				include(STYLESHEETPATH . '/'. Dvin_Wcql::template_path().'templates/list-empty.php');
			}else{
				require('templates/list-empty.php');
			}
		}
		echo '</div>';
		 return ob_get_clean();
	}
	
	/**
     * function Form, displays the form
	 * @param $atts Array - an associative array of attributes (preferences)
	 * @param $content  String -  the enclosed content
	 * @param $code String -  the shortcode name
	 * @code
	 * @static
     */
	static function form($atts, $content = null, $code = "") {
		global $dvin_wcql_settings,$dvin_qlist_products;
		if(!isset($_POST['qlist_process'])) {
			if(!isset($dvin_qlist_products) || (isset($dvin_qlist_products) && count($dvin_qlist_products)==0)) {			
	
				return;					
	
			}
		}
		
		if(isset($dvin_wcql_settings['use_gravity_forms']) && $dvin_wcql_settings['use_gravity_forms'] == 'on') {
			return do_shortcode('[gravityform id="'.$dvin_wcql_settings['gravity_form_select'].'" title=false description=false ajax="true"]');
		} else if(isset($dvin_wcql_settings['use_formidable_forms']) && $dvin_wcql_settings['use_formidable_forms'] == 'on') {
			return do_shortcode('[formidable id="'.$dvin_wcql_settings['formidable_form_select'].'"]');
		}else if(isset($dvin_wcql_settings['use_contactform7']) && $dvin_wcql_settings['use_contactform7'] == 'on') {
			return do_shortcode('[contact-form-7 id="'.$dvin_wcql_settings['contactform7_form_select'].'"]');
		}else {
			ob_start();
		
			if(file_exists(TEMPLATEPATH . '/'. Dvin_Wcql::template_path().'templates/form.php')) {
				include(TEMPLATEPATH . '/'. Dvin_Wcql::template_path().'templates/form.php');
			}elseif(file_exists(STYLESHEETPATH . '/'. Dvin_Wcql::template_path().'templates/form.php')) {
				include(STYLESHEETPATH . '/'. Dvin_Wcql::template_path().'templates/form.php');
			}else{
				echo '<div class="woocommerce">';
				include('templates/form.php');
				echo '</div>';
			}
			return ob_get_clean();
		}
	}
	
	/**
     * function button, displays quotelist button on shop page
	 * @param $atts Array - an associative array of attributes (preferences)
	 * @param $content  String -  the enclosed content
	 * @param $code String -  the shortcode name
	 * @codess
	 * @static
     */
 static function shopbutton($atts, $content = null, $code = "") {
        global $dvin_wcql_settings,$dvin_qlist_products;
		ob_start();
		echo '<div class="woocommerce">';
	 	echo dvin_addquotelist_button(true,$atts['product_id']);
		echo '</div>';
 		return ob_get_clean();
	}
    
    /**
     * function quotelist for the emails
	 * @param $atts Array - an associative array of attributes (preferences)
	 * @param $content  String -  the enclosed content
	 * @param $code String -  the shortcode name
	 * @code
	 * @static
     */
	static function quotelist($atts, $content = null, $code = "") {
        global $dvin_wcql_settings,$dvin_qlist_products;
        return serialize(get_qlist_table($atts));		
	}
	
       /**
     * function quotelisttable for the emails
	 * @param $atts Array - an associative array of attributes (preferences)
	 * @param $content  String -  the enclosed content
	 * @param $code String -  the shortcode name
	 * @code
	 * @static
     */
	static function quotelisttable($atts, $content = null, $code = "") {
        global $dvin_wcql_settings,$dvin_qlist_products;
         list(,$table) = get_qlist_table($atts);
         return $table;
	}
	
}
add_shortcode("dvin-wcql-listing", array("dvin_wcql_Shortcodes", "listing"));
add_shortcode("dvin-wcql-form", array("dvin_wcql_Shortcodes", "form"));
add_shortcode("dvin-wcql-shopbutton", array("dvin_wcql_Shortcodes", "shopbutton"));
add_shortcode("quotelist", array("dvin_wcql_Shortcodes", "quotelist"));
add_shortcode("quotelisttable", array("dvin_wcql_Shortcodes", "quotelisttable"));