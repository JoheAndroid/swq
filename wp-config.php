<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'FS_METHOD', 'direct' );
define( 'DB_NAME', 'swq' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'x(*,-30KEgX-z^~<mV0r_)8P<ScMg*nn(p<p!ngpC^eeTrPLk=aKInq2^:[YSLhr' );
define( 'SECURE_AUTH_KEY',  'RwF22WM%^+7l!$vf-srQ8X>@3`jfK%1VVQwCMiem>mQ&a:kP.qpn9z{5|fL>515W' );
define( 'LOGGED_IN_KEY',    'XBr qw%b.uA6yi8c3v}7X/XLHHLKD8vq0}THZ8IEtQL5r2+|sgg}EKRT&8jC^riK' );
define( 'NONCE_KEY',        'wNmX-,OoFn|#O*yVbEJl0K]>=]pKRDL<3=_1j7AQB<Sle}V~,cxe2T),a;jJDJ0`' );
define( 'AUTH_SALT',        'Wtij}t}7m.q_Vn#T{]^HUW:2R1EMfiqUQRP)s><0J^2TaY~6w^d7[[9&|q$y_<;i' );
define( 'SECURE_AUTH_SALT', '<;T6Sm6xCk,BI9D[qR*OK#Qb-IbBSeNeR##[6WMJ<@5>a$%om)_#:;EYO}i!,gj=' );
define( 'LOGGED_IN_SALT',   '~MJZZ2q`|WCTnO2Ib@5Hm&{]cE y6VA[Z`@X<rfD_nL>UcYhzMv>_^[j8|~Rc+,t' );
define( 'NONCE_SALT',       '{k{)=).|7{k,,x=A#nL^Y8<Mxp8d7)zRF~Y$]&)`*Q?.yeN%#hh9brQnaq~##[TG' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
